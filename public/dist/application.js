'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = '4th-year-project';
	var applicationModuleVendorDependencies = ['ngResource', 'ngCookies',  'ngAnimate',  'ngTouch',  'ngSanitize',  'ui.router', 'ui.bootstrap', 'ui.utils', 'chart.js'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('articles');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('boards');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('issues');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('projects');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		//Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		//Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		//Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);

'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('listArticles', {
			url: '/articles',
			templateUrl: 'modules/articles/views/list-articles.client.view.html'
		}).
		state('createArticle', {
			url: '/articles/create',
			templateUrl: 'modules/articles/views/create-article.client.view.html'
		}).
		state('viewArticle', {
			url: '/articles/:articleId',
			templateUrl: 'modules/articles/views/view-article.client.view.html'
		}).
		state('editArticle', {
			url: '/articles/:articleId/edit',
			templateUrl: 'modules/articles/views/edit-article.client.view.html'
		});
	}
]);
'use strict';

angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Articles',
	function($scope, $stateParams, $location, Authentication, Articles) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var article = new Articles({
				title: this.title,
				content: this.content
			});

			article.$save(function(response) {
				$location.path('articles/' + response._id);

				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};
	}
]);

'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('articles').factory('Articles', ['$resource',
	function($resource) {
		return $resource('articles/:articleId', {
			articleId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('boards').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Boards', 'boards', 'dropdown', '/boards(/create)?');
		Menus.addSubMenuItem('topbar', 'boards', 'List Boards', 'boards');
		//Menus.addSubMenuItem('topbar', 'boards', 'New Board', 'boards/create');
	}
]);

'use strict';

//Setting up route
angular.module('boards').config(['$stateProvider',
	function($stateProvider) {
		// Boards state routing
		$stateProvider.
		state('delete-column', {
			url: '/delete-column',
			templateUrl: 'modules/boards/views/delete-column.client.view.html'
		}).
		state('column-name', {
			url: '/column-name',
			templateUrl: 'modules/boards/views/column-name.client.view.html'
		}).
		state('create-column', {
			url: '/create-column',
			templateUrl: 'modules/boards/views/create-column.client.view.html'
		}).
		state('ticket', {
			url: '/ticket',
			templateUrl: 'modules/boards/views/ticket.client.view.html'
		}).
		state('listBoards', {
			url: '/boards',
			templateUrl: 'modules/boards/views/list-boards.client.view.html'
		}).
		state('createBoard', {
			url: '/boards/create',
			templateUrl: 'modules/boards/views/create-board.client.view.html'
		}).
		state('viewBoard', {
			url: '/boards/:boardId',
			templateUrl: 'modules/boards/views/view-board.client.view.html'
		}).
		state('editBoard', {
			url: '/boards/:boardId/edit',
			templateUrl: 'modules/boards/views/edit-board.client.view.html'
		});
	}
]);
'use strict';

var boardApp = angular.module('boards');

//==============================================================
// List Boards Controller
//==============================================================

boardApp.controller('BoardsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards',
	function($scope, $stateParams, $location, Authentication, Boards) {
		$scope.authentication = Authentication;

		// Find a list of Boards
		$scope.find = function() {
			$scope.boards = Boards.query();
		};

	}
]);

//==============================================================
// Create Board Controller
//==============================================================

boardApp.controller('BoardsCreateController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards',
	function($scope, $stateParams, $location, Authentication, Boards) {
		$scope.authentication = Authentication;

		$scope.handleDrop = function() {
			alert('Item has been dropped');
		};

		// Create new Board
		$scope.create = function() {
			// Create new Board object
			var board = new Boards ({
				name: this.name
			});

			// Redirect after save
			board.$save(function(response) {
				$location.path('boards/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

	}
]); // end of create board controller


//==============================================================
// Edit Board Controller
//==============================================================

boardApp.controller('BoardsEditController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards',
	function($scope, $stateParams, $location, Authentication, Boards) {
		$scope.authentication = Authentication;

		// Update existing Board
		$scope.update = function() {
			var board = $scope.board;

			board.$update(function() {
				$location.path('boards/' + board._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find existing Board
		$scope.findOne = function() {
			$scope.board = Boards.get({
				boardId: $stateParams.boardId
			});
		};

	}
]); // end of edit board controller

//==============================================================
// View Board Controller
//==============================================================

boardApp.controller('BoardsViewController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify', '$modal', '$log', '$window', '$state',
	function($scope, $stateParams, $location, Authentication, Boards, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify, $modal, $log, $window, $state) {

		$scope.authentication = Authentication;
		$scope.projectIssueArray = [];
		$scope.currentIssue;
		$scope.currentColumn;
		$scope.columnName;

		$scope.goToProject = function(){

			Boards.get({
				boardId: $stateParams.boardId
			}, function(boardResponse){
				Projects.query(function(projectsResponse){
					angular.forEach(projectsResponse, function(projectItem){
						if(projectItem._id === boardResponse.project._id){
							$location.path('projects/' + projectItem._id);
						}
					});
				});
			});

		};




// Remove existing Board
		$scope.remove = function(board) {
			if ( board ) {
				board.$remove();

				for (var i in $scope.boards) {
					if ($scope.boards [i] === board) {
						$scope.boards.splice(i, 1);
					}
				}
			} else {
				$scope.board.$remove(function() {
					$location.path('boards');
				});
			}
		};


		// Find existing Board
		$scope.findOne = function() {
			$scope.board = Boards.get({
				boardId: $stateParams.boardId
			});
		};

		// get a list of columns in the board
		$scope.getColumns = Boards.get({
			boardId: $stateParams.boardId
		}, function(response){
			$scope.columns = response.column;
		});

		// get a list of issues that belong to this project
		$scope.getIssues = Boards.get({
			boardId: $stateParams.boardId
		}, function(boardResponse){

			//$scope.projectIssues = ProjectIssues.query(function(response){
			//	angular.forEach(response, function(item){
            //
			//		if(item.project._id === boardResponse.project._id){
            //
			//			$scope.projectIssue = item;
			//			$scope.projectIssueArray.push(item);
			//		}
			//	});
            //
			//});

			angular.forEach(boardResponse.column, function(item){
				angular.forEach(item.issues, function(item2){

					$scope.projectIssueArray.push(item2);

				});

				//display each issue in column where it belongs

				//console.log($scope.projectIssueArray);
				//console.log(item.issues);
				$scope.projectIssue = item;

			});

		});

		$scope.getCurrentIssue = function(issue){
			$scope.currentIssue = issue;
			//console.log($scope.currentIssue);
		};

		$scope.getCurrentColumn = function(column){
			$scope.currentColumn = column;
			//console.log($scope.currentColumn);
		};

		//move item from one array to another when dropped
		$scope.handleDrop = function() {

			var board = Boards.get({
				boardId: $stateParams.boardId
			}, function(boardResponse){

				//delete item from column
				//console.log(boardResponse.column);

				angular.forEach(boardResponse.column, function(item){
					angular.forEach(item.issues, function(item2){
						if(item2._id === $scope.currentIssue._id){
							//console.log(item.issues);

							var index = item.issues.indexOf(item2);
							//console.log(index);

							//save item in new variable
							var savedItem = item2;

							//delete item form array
							item.issues.splice(index, 1); // uncomment once the code to move to new column is ready

							//add item to new column
							angular.forEach(boardResponse.column, function(item3){
								//console.log(item3);
								if(item3._id === $scope.currentColumn._id ){
									//console.log("the column to push data to is");
									//console.log(item3.issues);
									var columnIndex = boardResponse.column.indexOf(item3);

									if(boardResponse.column[columnIndex].issues.indexOf($scope.currentIssue._id) === -1){
										boardResponse.column[columnIndex].issues.push($scope.currentIssue._id);
										//console.log("Item Moved")
									}

									Array.prototype.last = function () {
										return this[this.length - 1];
									};

									if(item3 === boardResponse.column.last()){
										var issue = Issues.get({
											issueId: $scope.currentIssue._id
										}, function(issueResponse){
											//console.log(issueResponse);
											issueResponse.status  = 'Closed';

											issue.$update(function(updatedResponse){

											}, function(errorResponse){
												$scope.error = errorResponse.data.message;
											});
										});
									}

									//console.log(boardResponse.column.indexOf(item3));

									//item3.issues.push();
								}
							});
						}

					});

				});

				//boardResponse.column[0].issues.push(response._id)
				//console.log(boardResponse.column[0].issues);

				board.$update(function(updateResponse){
					//console.log(boardResponse);


					// reload the page
					//$location.path('boards/' + boardResponse._id).reload();
					//$window.location.reload()
					//$route.reload();
					$state.reload();

				}, function(errorResponse){
					$scope.error = errorResponse.data.message
				});
			});



		};


		// Open a modal window to add a column
		$scope.newColumn = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/boards/views/create-column.client.view.html',
				controller: ["$scope", "$modalInstance", function ($scope, $modalInstance) {

					$scope.ok = function (columnName) {

						var board = Boards.get({
							boardId: $stateParams.boardId
						}, function(boardResponse){
							console.log(boardResponse.column);

							console.log(columnName);

							boardResponse.column.splice(boardResponse.column.length - 1, 0 , {name: columnName});

							// add column
							board.$update(function(response){
								$state.reload();
							}, function(errorResponse){
								$scope.error = errorResponse.data.message;
							})
						});

						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.columnName;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		// open changeName modal window
		$scope.changeName = function (size, column) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/boards/views/column-name.client.view.html',
				controller: ["$scope", "$modalInstance", function($scope, $modalInstance){

					$scope.column = column;

					$scope.ok = function () {

						console.log(column);

						var board = Boards.get({
							boardId: $stateParams.boardId
						}, function(boardResponse){

							angular.forEach(boardResponse.column, function(item){
								if(item._id === column._id){

									item.name = column.name;

									board.$update(function(response){
										console.log(response);
									}, function(errorResponse){
										$scope.error = errorResponse.data.message;
									});
								}
							});


						});


						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		// Delete current column modal window
		$scope.deleteColumn = function (size, column) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/boards/views/delete-column.client.view.html',
				controller: ["$scope", "$modalInstance", function($scope, $modalInstance){

					$scope.column = column;

					$scope.ok = function () {


						var board = Boards.get({
							boardId: $stateParams.boardId
						}, function(boardResponse){

							angular.forEach(boardResponse.column, function(item){
								if(item._id === column._id){

									$scope.state = false;

									console.log(item.issues.length);

									if(item.issues.length === 0){
										boardResponse.column.splice(boardResponse.column.indexOf(item), 1);
										//console.log(boardResponse.column.indexOf(item));
										//console.log(item._id);
									} else {
										$scope.state = true;
									}

									board.$update(function(response){
										//console.log(response);
										$state.reload();
									}, function(errorResponse){
										$scope.error = errorResponse.data.message;
									});
								}
							});


						});


						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

	}
]); // End of view board controller


boardApp.controller('ItemController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify',
	function($scope, $stateParams, $location, Authentication, Boards, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify) {

	}
]);

'use strict';

angular.module('boards').controller('ViewBoardController', ['$scope',
	function($scope) {
		// View board controller logic
		// ...
	}
]);

'use strict';

angular.module('boards').directive('boards', [
	function() {
		return {
			template: '',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Boards directive logic
				// ...

				element.text('this is the boards directive');
			}
		};
	}
]).directive('draggable', function() {
	return function(scope, element) {
		// this gives us the native JS object
		var el = element[0];

		el.draggable = true;

		el.addEventListener(
			'dragstart',
			function(e) {
				e.dataTransfer.effectAllowed = 'move';
				e.dataTransfer.setData('Text', this.id);
				this.classList.add('drag');
				return false;
			},
			false
		);

		el.addEventListener(
			'dragend',
			function(e) {
				this.classList.remove('drag');
				return false;
			},
			false
		);
	}
}).directive('droppable', function() {
	return {
		scope: {
			drop: '&', // parent
			bin: '=' // bi-directional scope
		},
		link: function(scope, element) {
			// again we need the native object
			var el = element[0];

			el.addEventListener(
				'dragover',
				function(e) {
					e.dataTransfer.dropEffect = 'move';
					// allows us to drop
					if (e.preventDefault) e.preventDefault();
					this.classList.add('over');
					return false;
				},
				false
			);

			el.addEventListener(
				'dragenter',
				function(e) {
					this.classList.add('over');
					return false;
				},
				false
			);

			el.addEventListener(
				'dragleave',
				function(e) {
					this.classList.remove('over');
					return false;
				},
				false
			);

			el.addEventListener(
				'drop',
				function(e) {
					// Stops some browsers from redirecting.
					if (e.stopPropagation) e.stopPropagation();
					if(e.preventDefault) { e.preventDefault(); }

					var binId = this.id;

					this.classList.remove('over');

					var item = document.getElementById(e.dataTransfer.getData('Text'));
					this.appendChild(item);

					// call the passed drop function
					scope.$apply(function(scope) {
						var fn = scope.drop();
						if ('undefined' !== typeof fn) {
							fn(item.id, binId);
						}
					});

					return false;
				},
				false
			);
		}
	}
}).directive('addTickets', [
	function() {
		return {
			templateUrl: 'modules/boards/views/ticket.client.view.html',
			restrict: 'EA',
			scope: true,
			link: function postLink(scope, element, attrs) {
				if(scope.$first){
					// tickets to the first column

				}

			}
		};
	}
]);

'use strict';

//Boards service used to communicate Boards REST endpoints
angular.module('boards').factory('Boards', ['$resource',
	function($resource) {
		return $resource('boards/:boardId', { boardId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}

]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus',
	function($scope, Authentication, Menus) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
	}
]);

'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication', 'AssigneeIssues',
	function($scope, Authentication, AssigneeIssues) {

		$scope.authentication = Authentication;

		$scope.issues = [];

		AssigneeIssues.query(function (response) {
			angular.forEach(response, function (item) {
				//if(item.assignee == null){
				//	continue;
				//}
				if (item.assignee._id === $scope.authentication.user._id) {
					$scope.issues.push(item.issue);
				}
			});
		});

		$scope.query = {};

		$scope.type = {
			'design': 'Design',
			'analysis': 'Analysis',
			'documentation': 'Documentation',
			'testing': 'Testing',
			'coding': 'Coding'
		};

		$scope.checkBoxes = [
			{"type": "Analysis", checked: false},
			{"type": "Documentation", checked: false},
			{"type": "Coding", checked: false},
			{"type": "Testing", checked: false},
			{"type": "Design", checked: false}
		]
	}
]);


'use strict';

angular.module('core').filter('issuesFilter', ['Issues',
	function(Issues) {
		return function(input) {
			// Issues filter directive logic
			// ...



			Issues.query(function(issuesResponse){
				angular.forEach(issuesResponse, function(issuesResponseItem){

				});
			});

			return 'issuesFilter filter: ' + input;
		};
	}
]);

'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

// Configuring the Articles module
angular.module('issues').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Issues', 'issues', 'dropdown', '/issues(/create)?');
		Menus.addSubMenuItem('topbar', 'issues', 'List Issues', 'issues');
		Menus.addSubMenuItem('topbar', 'issues', 'New Issue', 'issues/create');
	}
]);
'use strict';

//Setting up route
angular.module('issues').config(['$stateProvider',
	function($stateProvider) {
		// Issues state routing
		$stateProvider.
		state('change-project', {
			url: '/change-project',
			templateUrl: 'modules/issues/views/change-project.client.view.html'
		}).
		state('change-assignee', {
			url: '/change-assignee',
			templateUrl: 'modules/issues/views/change-assignee.client.view.html'
		}).
		state('log-work', {
			url: '/log-work',
			templateUrl: 'modules/issues/views/log-work.client.view.html'
		}).
		state('move-issue', {
			url: '/move-issue/:boardId',
			templateUrl: 'modules/issues/views/move-issue.client.view.html'
		}).
		state('listIssues', {
			url: '/issues',
			templateUrl: 'modules/issues/views/list-issues.client.view.html'
		}).
		state('createIssue', {
			url: '/issues/create',
			templateUrl: 'modules/issues/views/create-issue.client.view.html'
		}).
		state('viewIssue', {
			url: '/issues/:issueId',
			templateUrl: 'modules/issues/views/view-issue.client.view.html'
		}).
		state('editIssue', {
			url: '/issues/:issueId/edit',
			templateUrl: 'modules/issues/views/edit-issue.client.view.html'
		});
	}
]);

'use strict';

angular.module('issues').controller('ChangeAssigneeController', ['$scope',
	function($scope) {
		// Controller Logic
		// ...
	}
]);
'use strict';

angular.module('issues').controller('ChangeProjectController', ['$scope',
	function($scope) {
		// Controller Logic
		// ...
	}
]);
'use strict';

angular.module('issues').controller('CreateIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues',  'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Boards',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Boards) {

		$scope.authentication = Authentication;
		$scope.typeArray = ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'];
		$scope.priorityArray = ['Low', 'Medium', 'High'];
		$scope.projects = [];
		Projects.query(function(projectResponse){
			angular.forEach(projectResponse, function(item){
				if(item.status !== 'Closed'){
					$scope.projects.push(item);
				}
			});
		});
		$scope.listUsers = [];
		UsersList.query(function(response){
			angular.forEach(response, function(item){
				if(item.status !== 'inactive'){
					$scope.listUsers.push(item);
				}
			});
		});

		//==============================================================================================
		// Create List that will be displayed when members select the button to assign
		//==============================================================================================

					$scope.userList = [];
					$scope.userList.push($scope.authentication.user);

						UsersList.query(function(assigneeIssuesResponse){
							angular.forEach(assigneeIssuesResponse, function(item){
								if(item.roles === 'admin' || item.roles === 'manager'){

									if(item.status !== 'inactive'){
										$scope.userList.push(item);
									}


								}
							});
							//console.log($scope.userList);
						});


		$scope.selectProject = function(){
			return $scope.selectedProject;
		};

		// Create new Issue
		$scope.create = function() {

			if($scope.selectedProject == undefined){
				$scope.error = 'Please Choose Project';
			}


			// Create new Issue object
			var issue = new Issues ({
				name: $scope.selectedProject.name,
				created: this.created,
				reporter: $scope.authentication.user._id,
				issueType: this.issueType,
				summary: this.summary,
				priority: this.priority,
				description: this.description,
				attachment: this.attachment,
				estimate: this.estimate,
				company: $scope.authentication.user.company
			});

			// Redirect after save
			issue.$save(function(response) {


				if($scope.selectedAssignee == undefined){
					$scope.error = 'Please Choose Assignee';
					return;
				}

				if($scope.selectedProject == undefined){
					$scope.error = 'Please Choose Project';
					return;
				}

				// create new assigneeIssue item
				var assigneeIssue = new AssigneeIssues({
					issue: response._id,
					assignee: $scope.selectedAssignee
				});

				assigneeIssue.$save(function(response){

				}, function(errorResponse){
					$scope.error = errorResponse.data.message;
				});

				// create new projectIssue item
				var projectIssue = new ProjectIssues({
					issue: response._id,
					project: $scope.selectedProject._id
				});

				projectIssue.$save(function(response){

				}, function(errorResponse){
					$scope.error = errorResponse.data.message;
				});

				//add issue created to to To Do column in board

				// get all board and loop checking for the board belonging to the current project
				var getBoards = Boards.query(function(boardsResponse){
					angular.forEach(boardsResponse, function(item){
						if(item.project._id === projectIssue.project){



							var board = Boards.get({
								boardId: item._id
							}, function(boardResponse){
								boardResponse.column[0].issues.push(response._id)
								//console.log(boardResponse.column[0].issues);

								board.$update(function(updateResponse){
									console.log(boardResponse.column[0].issues);
								}, function(errorResponse){
									$scope.error = errorResponse.data.message
								});
							});

						}
					});
				});



				$location.path('issues/' + response._id);
				$scope.name = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};
	}
]);

'use strict';

angular.module('issues').controller('EditIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues) {


		$scope.authentication = Authentication;
		$scope.typeArray = ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'];
		$scope.priorityArray = ['Low', 'Medium', 'High'];
		$scope.projects = Projects.query();
		$scope.listUsers = UsersList.query();


		//==============================================================================================
		// Create List that will be displayed when members select the button to assign
		//==============================================================================================
		$scope.userList = [];

		AssigneeIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id === $stateParams.issueId){
					//console.log(item.assignee);

					$scope.userList.push($scope.authentication.user);

					ProjectIssues.query(function(projectIssuesResponse){
						angular.forEach(projectIssuesResponse, function(item){
							if(item.issue._id === $stateParams.issueId){
								Projects.get({
									projectId: item.project._id
								}, function(projectResponse){
									//console.log(projectResponse.lead);
									$scope.userList.push(projectResponse.lead);
								});
							}
						});
					});

				}
			});
			//console.log($scope.userList);
		});

		var assigneeIssue = {};

		$scope.assigneeIssues = AssigneeIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id === $stateParams.issueId){
					//console.log(item);
					$scope.assigneeIssue = item;
					//console.log($scope.assigneeIssue);
					assigneeIssue = AssigneeIssues.get({
						assigneeIssueId: item._id
					});
				}
			});
		});

		var projectIssue = {};

		$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
			angular.forEach(projectIssuesResponse, function(item){
				if(item.issue._id === $stateParams.issueId){
					console.log(item);
					$scope.projectIssue = item;
					projectIssue = ProjectIssues.get({
						projectIssueId: item._id
					});
					//console.log(projectIssue);
				}
			});
		});

		//console.log(projectIssue);
		//console.log(assigneeIssue);


		// Update existing Issue
		$scope.update = function() {
			var issue = $scope.issue;

			assigneeIssue.$update(function(){

			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			});

			projectIssue.$update(function(){

			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			});

			issue.$update(function() {
				$location.path('issues/' + issue._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Find existing Issue
		$scope.findOne = function() {
			$scope.issue = Issues.get({
				issueId: $stateParams.issueId
			});
		};


		// Remove existing Issue
		$scope.remove = function(issue) {
			if ( issue ) {
				issue.$remove();

				for (var i in $scope.issues) {
					if ($scope.issues [i] === issue) {
						$scope.issues.splice(i, 1);
					}
				}
			} else {
				$scope.issue.$remove(function() {
					$location.path('issues');
				});
			}
		};

	}

]);

'use strict';

// Issues controller
angular.module('issues').controller('IssuesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues',
	function($scope, $stateParams, $location, Authentication, Issues) {
		$scope.authentication = Authentication;









	}
]);

'use strict';

angular.module('issues').controller('ListIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues',
	function($scope, $stateParams, $location, Authentication, Issues) {
		// Find a list of Issues
		$scope.find = function() {
			$scope.issues = Issues.query();
		};
	}
]);

'use strict';

angular.module('issues').controller('LogWorkController', ['$scope',
	function($scope) {
		// Controller Logic
		// ...
	}
]);
'use strict';

angular.module('issues').controller('ViewIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify', '$modal', '$log', 'Boards', 'Logs', 'Budgets', '$state',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify, $modal, $log, Boards, Logs, Budgets, $state) {

		$scope.authentication = Authentication;
		$scope.typeArray = ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'];
		$scope.priorityArray = ['Low', 'Medium', 'High'];
		$scope.projects = Projects.query();
		$scope.listUsers = [];
		UsersList.query(function(response){
			angular.forEach(response, function(item){
				if(item.status !== 'inactive'){
					$scope.listUsers.push(item);
				}
			});
		});
		$scope.projectIssues = ProjectIssues.query();
		$scope.items = [];
		$scope.issue = Issues.get({
			issueId: $stateParams.issueId
		}, function(issueResponse){
			$scope.max =  issueResponse.estimate;
		});




		$scope.assigneeIssues = AssigneeIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id === $stateParams.issueId){
					$scope.assigneeIssue = item;
				}
			});
		});

		$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
			angular.forEach(projectIssuesResponse, function(item){
				if(item.issue._id === $stateParams.issueId){
					$scope.projectIssue = item;
				}
			});
		});

		//==============================================================================================
		// Create List that will be displayed when members select the button to assign
		//==============================================================================================

		$scope.userList = [];
		$scope.userList.push($scope.authentication.user);

		UsersList.query(function(assigneeIssuesResponse){
			angular.forEach(assigneeIssuesResponse, function(item){
				if(item.roles === 'admin' || item.roles === 'manager'){

					if(item.status !== 'inactive'){
						$scope.userList.push(item);
					}


				}
			});
			//console.log($scope.userList);
		});



		for(var i = 0; i < $scope.assigneeIssues.length; i++){
			//if($scope.assigneeIssues[i].issue._id == $stateParams.issueId){
			//	$scope.currentIssue = $scope.assigneeIssues[i].issue._id;
			//}
			//console.log($scope.assigneeIssues[i].issue._id );
		}


		// Find existing Issue
		$scope.findOne = function() {
			$scope.issue = Issues.get({
				issueId: $stateParams.issueId
			});
		};

		$scope.addComment = function(){
			var comment = new Comments({
				user: $scope.authentication.user._id,
				issue: $stateParams.issueId,
				comment: $scope.comment
			});

			comment.$save(function(response){
				$state.reload();
				//Notify.sendMsg('NewComment', {'id': response._id});
                //
				//Notify.getMsg('NewComment', function(event, data){
				//	$scope.comments = Comments.query();
				//});

				// clear the comment text area
				$scope.comment = '';

			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Issues
		//$scope.findComments = function() {
		//	$scope.comments = Comments.query();
		//};

		$scope.commentsArray = [];

		$scope.comments = Comments.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id == $stateParams.issueId){
					$scope.commentsArray.push(item);

				}
			});
		});

		$scope.log;

		// log work time
		//$scope.logWork = function(){
        //
		//		//retrieve the log info from database and add the current log to it
		//		var issue = Issues.get({
		//			issueId: $stateParams.issueId
		//		}, function(response){
        //
        //
		//			$scope.issue.log = $scope.issue.log + response.log;
        //
		//			issue.$update(function() {
		//				//$location.path('articles/' + article._id);
		//			}, function(errorResponse) {
		//				$scope.error = errorResponse.data.message;
		//			});
		//		});
        //
        //
		//};

		//// Find existing Assignee Issue
		//$scope.findAssigneeIssue = function() {
		//	$scope.assigneeIssue = AssigneeIssues.get({
		//		assigneeIssueId: $stateParams.assigneeIssueId
		//	});
		//};

		//// Find existing Project Issue
		//$scope.findProjectIssue = function() {
		//	$scope.projectIssue = ProjectIssues.get({
		//		issueId: $stateParams.issueId
		//	});
		//};

		//=================================================================================================
		//Run this code so as to get the column names
		//=================================================================================================
		var projectIssues = ProjectIssues.query(function(projectIssuesResponse){
			angular.forEach(projectIssuesResponse, function(projectIssueItem){
				if(projectIssueItem.issue._id === $stateParams.issueId){
					//$scope.projectIssue = projectIssueItem;
					//console.log($scope.projectIssue.project._id);
					//console.log(projectIssueItem);

					//find all the boards
					var boards = Boards.query(function(boardsResponse){
						angular.forEach(boardsResponse, function(boardsItem){
							//console.log(boardsItem.project._id);
							if(boardsItem.project._id === projectIssueItem.project._id){

								//========================================================================
								//Handle Moving Issue From One Column to Another Here
								//========================================================================

								// Loop through all the  columns
								angular.forEach(boardsItem.column, function(columnItem){
									// loop through all issues in a column
									angular.forEach(columnItem.issues, function(issuesItem){
										if(issuesItem === $stateParams.issueId){
											//console.log(boardsItem.column);

											Array.prototype.last = function () {
												return this[this.length - 1];
											};

											$scope.checkColumns = function(){

												$scope.ifLast = false;

												if(columnItem === boardsItem.column.last()){
													$scope.ifLast = true;
												}

												return $scope.ifLast;
											};
											$scope.columns = boardsItem.column;
											$scope.currentColumn = columnItem.name;
											//console.log(boardsItem.column.indexOf(columnItem));

											//console.log(boardsItem.column.indexOf(columnItem + 1));
											if(columnItem !== boardsItem.column.last()){
												$scope.nextColumn = boardsItem.column[boardsItem.column.indexOf(columnItem) + 1].name;
											}

										}
									});

								});
							}
						});
					});

				}
			});
		});




		//===================================================================================================================
		//Code to log work
		//===================================================================================================================

		$scope.moveIssue = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/move-issue.client.view.html',
				controller: ["$scope", "$modalInstance", "$state", "Authentication", function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (assigneeIssue) {

						var projectIssues = ProjectIssues.query(function(projectIssuesResponse){
							angular.forEach(projectIssuesResponse, function(projectIssueItem){
								if(projectIssueItem.issue._id === $stateParams.issueId){
									//$scope.projectIssue = projectIssueItem;
									//console.log($scope.projectIssue.project._id);
									//console.log(projectIssueItem);

									//find all the boards
									var boards = Boards.query(function(boardsResponse){
										angular.forEach(boardsResponse, function(boardsItem){
											//console.log(boardsItem.project._id);
											if(boardsItem.project._id === projectIssueItem.project._id){

												var board = Boards.get({
													boardId: boardsItem._id
												}, function(boardResponse){

													angular.forEach(boardResponse.column, function(columnItem){
														angular.forEach(columnItem.issues, function(issuesItem){
															if(issuesItem._id === $stateParams.issueId){

																if(columnItem !== boardResponse.column.last()){
																	//delete item from the column
																	columnItem.issues.splice(columnItem.issues.indexOf(issuesItem), 1);

																	//move item to new column
																	boardResponse.column[boardResponse.column.indexOf(columnItem) + 1].issues.push(issuesItem._id);
																}

																//console.log(boardResponse.column.last()._id);
																//console.log(columnItem._id);

																if((boardResponse.column.indexOf(columnItem) + 1) === boardResponse.column.indexOf(boardResponse.column.last())){
																	console.log('issue is in last column');
																	var issue = Issues.get({
																		issueId: $stateParams.issueId
																	}, function(issueResponse){
																		console.log(issueResponse);
																		issueResponse.status  = 'Closed';

																		issue.$update(function(updatedResponse){

																		}, function(errorResponse){
																			$scope.error = errorResponse.data.message;
																		});
																	});
																}

															}
														});
													});

														board.$update(function(updateResponse){
															//console.log(boardResponse.column);

															// reload the page
															//$location.path('boards/' + boardResponse._id).reload();
															//$window.location.reload()
															//$route.reload();
															$state.reload();

														}, function(errorResponse){
															$scope.error = errorResponse.data.message
														});



												});

											}
										});
									});

								}
							});
						});



						//retrieve the log info from database and add the current log to it
						var issue = Issues.get({
							issueId: $stateParams.issueId
						}, function(response){

							issue.$update(function(response) {
								//$location.path('articles/' + article._id);
							}, function(errorResponse) {
								$scope.error = errorResponse.data.message;
							});
						});

						$scope.assignToLead = function(){
							//console.log("hhahhahhaha");
						}


						assigneeIssue.$update(function(updateResponse){

						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});

						//var log = new Logs({
						//	issue: $stateParams.issueId,
						//	user: $scope.authentication.user._id,
						//	time: this.log
						//});

						//log.$save(function(logResponse){
                        //
						//	//console.log(logResponse);
                        //
						//	$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
						//		angular.forEach(projectIssuesResponse, function(item){
						//			if(item.issue._id === $stateParams.issueId){
						//				$scope.projectIssue = item;
						//				//console.log(item);
						//				//console.log($scope.authentication.user._id);
						//				//console.log(item.project._id);
						//				//console.log(item.issue.issueType);
                        //
						//				var cost = (function(){
						//					// check where the item belongs to in the
						//					switch(item.issue.issueType){
						//						case "Design":
						//							return logResponse.time * item.project.charge.design;
						//							break;
						//						case "Analysis":
						//							return logResponse.time * item.project.charge.analysis;
						//							break;
						//						case "Coding":
						//							return logResponse.time * item.project.charge.coding;
						//							break;
						//						case "Documentation":
						//							return logResponse.time * item.project.charge.documentation;
						//							break;
						//						case "Bug":
						//							return logResponse.time * item.project.charge.coding;
						//							break;
						//					}
                        //
						//				});
                        //
                        //
						//				var budget = new Budgets({
						//					user: $scope.authentication.user._id,
						//					projectid: item.project._id,
						//					issueType: item.issue.issueType,
						//					cost: cost()
						//				});
                        //
						//				budget.$save(function(budgetResponse){
                        //
						//					//console.log(budgetResponse);
                        //
						//				}, function(errorResponse){
						//					$scope.error = errorResponse.data.message;
						//				});
						//			}
						//		});
						//	});
                        //
						//	//var budget = new Budgets({
						//	//	user: $scope.authentication.user._id,
						//	//	project:
						//	//	issueType:
						//	//	cost:
						//	//});
                        //
                        //
						//}, function(errorResponse){
						//	$scope.error = errorResponse.data.message;
						//});

						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};


		$scope.logTotal = 0;
		var logArray = [];

		Logs.query(function(logsResponse){
			angular.forEach(logsResponse, function(logsResponseItem){
				if(logsResponseItem.issue._id === $stateParams.issueId){
					logArray.push(logsResponseItem);
					$scope.logTotal = $scope.logTotal + logsResponseItem.time;
				}
			});
			//console.log(logArray);
			//for(var i = 0; i < logArray.length; i++){
			//	logTotal = logTotal + logArray[i].time;
			//	console.log(logArray[i].time);
			//}
			//console.log($scope.logTotal);
			$scope.dynamic = $scope.logTotal;

		});

		//change assignee
		$scope.changeAssignee = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/change-assignee.client.view.html',
				controller: ["$scope", "$modalInstance", "$state", "Authentication", function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (assigneeIssue, newComment) {

						var comment = new Comments({
							user: $scope.authentication.user._id,
							issue: $stateParams.issueId,
							comment: newComment
						});

						comment.$save(function(response){
							//$state.reload();
							//Notify.sendMsg('NewComment', {'id': response._id});
							//
							//Notify.getMsg('NewComment', function(event, data){
							//	$scope.comments = Comments.query();
							//});

							// clear the comment text area
							$scope.comment = '';

						}, function(errorResponse){
							//$scope.error = errorResponse.data.message;
						});

						// Find a list of Issues
						//$scope.findComments = function() {
						//	$scope.comments = Comments.query();
						//};

						$scope.commentsArray = [];

						$scope.comments = Comments.query(function(response){
							angular.forEach(response, function(item){
								if(item.issue._id == $stateParams.issueId){
									$scope.commentsArray.push(item);

								}
							});
						});

						assigneeIssue.$update(function(updateResponse){
							$state.reload();
						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});

						$modalInstance.close();

					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});


			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		//change project
		$scope.changeProject = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/change-project.client.view.html',
				controller: ["$scope", "$modalInstance", "$state", "Authentication", function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (projectIssue, project) {

						//console.log(projectIssue._id);

					ProjectIssues.query(function(projectIssuesResponse){
							angular.forEach(projectIssuesResponse, function(projectIssueItem){
								if(projectIssueItem.issue._id === $stateParams.issueId){
									//$scope.projectIssue = projectIssueItem;
									//console.log($scope.projectIssue.project._id);
									//console.log(projectIssueItem);

									//find all the boards
									var boards = Boards.query(function(boardsResponse){
										angular.forEach(boardsResponse, function(boardsItem){
											//console.log(boardsItem.project._id);
											if(boardsItem.project._id === projectIssueItem.project._id){

												var board = Boards.get({
													boardId: boardsItem._id
												}, function(boardResponse){

													angular.forEach(boardResponse.column, function(columnItem){
														angular.forEach(columnItem.issues, function(issuesItem){
															if(issuesItem._id === $stateParams.issueId){

																if(columnItem !== boardResponse.column.last()){
																	//delete item from the column
																	columnItem.issues.splice(columnItem.issues.indexOf(issuesItem), 1);

																	//move item to new column
																	boardResponse.column[boardResponse.column.indexOf(columnItem) + 1].issues.push(issuesItem._id);
																}

																//console.log(boardResponse.column.last()._id);
																//console.log(columnItem._id);

																if((boardResponse.column.indexOf(columnItem) + 1) === boardResponse.column.indexOf(boardResponse.column.last())){
																	console.log('issue is in last column');
																	var issue = Issues.get({
																		issueId: $stateParams.issueId
																	}, function(issueResponse){
																		console.log(issueResponse);
																		issueResponse.status  = 'Closed';

																		issue.$update(function(updatedResponse){

																		}, function(errorResponse){
																			$scope.error = errorResponse.data.message;
																		});
																	});
																}

															}
														});
													});

													board.$update(function(updateResponse){
														//console.log(boardResponse.column);

														// reload the page
														//$location.path('boards/' + boardResponse._id).reload();
														//$window.location.reload()
														//$route.reload();
														$state.reload();

													}, function(errorResponse){
														$scope.error = errorResponse.data.message
													});



												});

											}
										});
									});

								}
							});
						});

						console.log(project);

						projectIssue.$update(function(updateResponse){

							var boards = Boards.query(function(boardsResponse){
								angular.forEach(boardsResponse, function(boardResponseItem){
									console.log(boardsResponse);
								});
							});
							console.log(updateResponse);
							$state.reload();
							$modalInstance.close();
						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});



					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});


			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		//log work
		$scope.logWork = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/log-work.client.view.html',
				controller: ["$scope", "$modalInstance", "$state", "Authentication", function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (assigneeIssue, newComment) {

						//var projectIssues = ProjectIssues.query(function(projectIssuesResponse){
						//	angular.forEach(projectIssuesResponse, function(projectIssueItem){
						//		if(projectIssueItem.issue._id === $stateParams.issueId){
						//			//$scope.projectIssue = projectIssueItem;
						//			//console.log($scope.projectIssue.project._id);
						//			//console.log(projectIssueItem);
                        //
						//			//find all the boards
						//			var boards = Boards.query(function(boardsResponse){
						//				angular.forEach(boardsResponse, function(boardsItem){
						//					//console.log(boardsItem.project._id);
						//					if(boardsItem.project._id === projectIssueItem.project._id){
                        //
						//						var board = Boards.get({
						//							boardId: boardsItem._id
						//						}, function(boardResponse){
                        //
						//							angular.forEach(boardResponse.column, function(columnItem){
						//								angular.forEach(columnItem.issues, function(issuesItem){
						//									if(issuesItem._id === $stateParams.issueId){
                        //
						//										if(columnItem !== boardResponse.column.last()){
						//											//delete item from the column
						//											columnItem.issues.splice(columnItem.issues.indexOf(issuesItem), 1);
                        //
						//											//move item to new column
						//											boardResponse.column[boardResponse.column.indexOf(columnItem) + 1].issues.push(issuesItem._id);
						//										}
                        //
						//										//console.log(boardResponse.column.last()._id);
						//										//console.log(columnItem._id);
                        //
						//										if((boardResponse.column.indexOf(columnItem) + 1) === boardResponse.column.indexOf(boardResponse.column.last())){
						//											console.log('issue is in last column');
						//											var issue = Issues.get({
						//												issueId: $stateParams.issueId
						//											}, function(issueResponse){
						//												console.log(issueResponse);
						//												issueResponse.status  = 'Closed';
                        //
						//												issue.$update(function(updatedResponse){
                        //
						//												}, function(errorResponse){
						//													$scope.error = errorResponse.data.message;
						//												});
						//											});
						//										}
                        //
						//									}
						//								});
						//							});
                        //
						//							board.$update(function(updateResponse){
						//								//console.log(boardResponse.column);
                        //
						//								// reload the page
						//								//$location.path('boards/' + boardResponse._id).reload();
						//								//$window.location.reload()
						//								//$route.reload();
						//								$state.reload();
                        //
						//							}, function(errorResponse){
						//								$scope.error = errorResponse.data.message
						//							});
                        //
                        //
                        //
						//						});
                        //
						//					}
						//				});
						//			});
                        //
						//		}
						//	});
						//});

							var comment = new Comments({
								user: $scope.authentication.user._id,
								issue: $stateParams.issueId,
								comment: newComment
							});

							comment.$save(function(response){
								//$state.reload();
								//Notify.sendMsg('NewComment', {'id': response._id});
								//
								//Notify.getMsg('NewComment', function(event, data){
								//	$scope.comments = Comments.query();
								//});

								// clear the comment text area
								$scope.comment = '';

							}, function(errorResponse){
								//$scope.error = errorResponse.data.message;
							});

						// Find a list of Issues
						//$scope.findComments = function() {
						//	$scope.comments = Comments.query();
						//};

						$scope.commentsArray = [];

						$scope.comments = Comments.query(function(response){
							angular.forEach(response, function(item){
								if(item.issue._id == $stateParams.issueId){
									$scope.commentsArray.push(item);

								}
							});
						});



						//retrieve the log info from database and add the current log to it
						var issue = Issues.get({
							issueId: $stateParams.issueId
						}, function(response){

							issue.$update(function(response) {
								//$location.path('articles/' + article._id);
							}, function(errorResponse) {
								$scope.error = errorResponse.data.message;
							});
						});

						$scope.assignToLead = function(){
							//console.log("hhahhahhaha");
						}


						assigneeIssue.$update(function(updateResponse){

						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});

						var log = new Logs({
							issue: $stateParams.issueId,
							user: $scope.authentication.user._id,
							time: this.log
						});

						log.$save(function(logResponse){

							//console.log(logResponse);

							$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
								angular.forEach(projectIssuesResponse, function(item){
									if(item.issue._id === $stateParams.issueId){
										$scope.projectIssue = item;
										//console.log(item);
										//console.log($scope.authentication.user._id);
										//console.log(item.project._id);
										//console.log(item.issue.issueType);

										var cost = (function(){
											// check where the item belongs to in the
											switch(item.issue.issueType){
												case "Design":
													return logResponse.time * item.project.charge.design;
													break;
												case "Analysis":
													return logResponse.time * item.project.charge.analysis;
													break;
												case "Coding":
													return logResponse.time * item.project.charge.coding;
													break;
												case "Documentation":
													return logResponse.time * item.project.charge.documentation;
													break;
												case "Bug":
													return logResponse.time * item.project.charge.coding;
													break;
											}

										});


										var budget = new Budgets({
											user: $scope.authentication.user._id,
											projectid: item.project._id,
											issueType: item.issue.issueType,
											cost: cost(),
											hours: logResponse.time
										});

										budget.$save(function(budgetResponse){

											//console.log(budgetResponse);

										}, function(errorResponse){
											$scope.error = errorResponse.data.message;
										});
									}
								});
							});

							//var budget = new Budgets({
							//	user: $scope.authentication.user._id,
							//	project:
							//	issueType:
							//	cost:
							//});

							if(logResponse){
								$modalInstance.close();
								$state.reload();
							}


						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});


					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				}],
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};
	}
]);

'use strict';

angular.module('issues').directive('issues', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Issues directive logic
				// ...

				element.text('this is the issues directive');
			}
		};
	}
]).directive('ngEnter', function(){
	return function(scope, element, attrs){
		element.bind('keydown keypress', function(event){
			if(event.which === 13){
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
}).directive('renderOnBlur', function() {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, elm, attrs, ctrl) {
			elm.bind('blur', function() {
				var viewValue = ctrl.$modelValue;
				for (var i in ctrl.$formatters) {
					viewValue = ctrl.$formatters[i](viewValue);
				}
				ctrl.$viewValue = viewValue;
				ctrl.$render();
			});
		}
	};
});;

'use strict';

//Issues service used to communicate Issues REST endpoints
angular.module('issues').factory('Issues', ['$resource',
	function($resource) {
		return $resource('issues/:issueId', { issueId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Upload', ['$resource',
	function($resource) {
		return $resource('/upload/:filename', { filename: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('AssigneeIssues', ['$resource',
	function($resource) {
		return $resource('assigneeIssues/:assigneeIssueId', { assigneeIssueId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			query:  {
				method:'GET', isArray:true
			}
		});
	}
]).factory('ProjectIssues', ['$resource',
	function($resource) {
		return $resource('projectIssues/:projectIssueId', { projectIssueId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			query:  {
				method:'GET', isArray:true
			}
		});
	}
]).factory('Comments', ['$resource',
	function($resource) {
		return $resource('comments/:commentId', { commentId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Notify', ['$rootScope', function($rootScope) {
		var notify = [];

		notify.sendMsg = function(msg, data){
			data = data || {};
			$rootScope.$emit(msg, data);

			console.log('message sent');
		}

		notify.getMsg = function(msg, func, scope){
			var unbind = $rootScope.$on(msg, func);

			if(scope){
				scope.$on('destroy', unbind);
			}
		}

		return notify;
	}
]).factory('Logs', ['$resource',
	function($resource) {
		return $resource('logs/:logId', { logId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);

'use strict';

// Configuring the Articles module
angular.module('projects').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Projects', 'projects', 'dropdown', '/projects(/create)?');
		Menus.addSubMenuItem('topbar', 'projects', 'List Projects', 'projects');
		Menus.addSubMenuItem('topbar', 'projects', 'New Project', 'projects/create');
	}
]);
'use strict';

//Setting up route
angular.module('projects').config(['$stateProvider',
	function($stateProvider) {
		// Projects state routing
		$stateProvider.
		state('work-logged', {
			url: '/projects/:projectId/work-logged',
			templateUrl: 'modules/projects/views/work-logged.client.view.html'
		}).
		state('budget', {
			url: '/projects/:projectId/budget',
			templateUrl: 'modules/projects/views/budget.client.view.html'
		}).
		state('project-issue', {
			url: '/projects/:projectId/project-issues',
			templateUrl: 'modules/projects/views/project-issue.client.view.html'
		}).
		state('project-cost', {
			url: '/project-cost/:projectId',
			templateUrl: 'modules/projects/views/project-cost.client.view.html'
		}).
		state('listProjects', {
			url: '/projects',
			templateUrl: 'modules/projects/views/list-projects.client.view.html'
		}).
		state('createProject', {
			url: '/projects/create',
			templateUrl: 'modules/projects/views/create-project.client.view.html'
		}).
		state('viewProject', {
			url: '/projects/:projectId',
			templateUrl: 'modules/projects/views/view-project.client.view.html'
		}).
		state('editProject', {
			url: '/projects/:projectId/edit',
			templateUrl: 'modules/projects/views/edit-project.client.view.html'
		});
	}
]);

'use strict';

angular.module('projects').controller('BudgetController', ['$scope', 'UsersList', '$stateParams', 'Projects', 'Authentication', '$location', 'Users', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify', '$modal', '$log', 'Boards', 'Logs', 'Budgets', 'BudgetAgg', 'BudgetWeek', 'BudgetMonth', 'BudgetAggDate',
	function($scope, UsersList, $stateParams, Projects, Authentication, $location, Users, AssigneeIssues, ProjectIssues, Comments, Notify, $modal, $log, Boards, Logs, Budgets, BudgetAgg, BudgetWeek, BudgetMonth, BudgetAggDate) {

		$scope.authentication = Authentication;


			$scope.project = Projects.get({
				projectId: $stateParams.projectId
			});

		Budgets.query({project: $stateParams.projectId}, function(response){
			//console.log(response);
		});


		BudgetAgg.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabel = [];
			$scope.budgetValue = []
			angular.forEach(response, function(responseItem){
				$scope.budgetLabel.push(responseItem._id);
				$scope.budgetValue.push(responseItem.total);
			});
			//console.log(response);

			$scope.budgetObj = {};

			for (var i = 0; i < $scope.budgetLabel.length; i++){
				$scope.budgetObj[$scope.budgetLabel[i]] = $scope.budgetValue[i];
			}

		});

		//Query budget aggregate with a date parameter for last week
		console.log(moment().subtract(7, 'days').calendar());

		BudgetWeek.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabelDate7 = [];
			$scope.budgetValueDate7 = [];
			angular.forEach(response, function(responseItem){
				$scope.budgetLabelDate7.push(responseItem._id);
				$scope.budgetValueDate7.push(responseItem.total);
			});
			//console.log(response);
			console.log($scope.budgetLabelDate7);
			console.log($scope.budgetValueDate7);

			$scope.budgetObjDate7= {};

			for (var i = 0; i < $scope.budgetLabelDate7.length; i++){
				$scope.budgetObjDate7[$scope.budgetLabelDate7[i]] = $scope.budgetValueDate7[i];
			}

			console.log($scope.budgetObjDate7);
		});

		//Query budget aggregate with a date parameter for last month
		console.log(moment().subtract(30, 'days').calendar());

		BudgetMonth.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabelDate30 = [];
			$scope.budgetValueDate30 = [];
			angular.forEach(response, function(responseItem){
				$scope.budgetLabelDate30.push(responseItem._id);
				$scope.budgetValueDate30.push(responseItem.total);
			});
			//console.log(response);
			console.log($scope.budgetLabelDate30);
			console.log($scope.budgetValueDate30);

			$scope.budgetObjDate30 = {};

			for (var i = 0; i < $scope.budgetLabelDate30.length; i++){
				$scope.budgetObjDate30[$scope.budgetLabelDate30[i]] = $scope.budgetValueDate30[i];
			}

			console.log($scope.budgetObjDate30);
		});



		$scope.dt = new Date();

		//$scope.today();

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		$scope.disabled = function(date, mode) {
			return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.toggleMin = function() {
			$scope.minDate = $scope.minDate ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];

		$scope.costToday = function(dt){
			BudgetAggDate.query({
				budgetAggId: $stateParams.projectId,
				date: dt
			}, function(response){
				$scope.budgetLabelToday = [];
				$scope.budgetValueToday = [];
				angular.forEach(response, function(responseItem){
					$scope.budgetLabelToday.push(responseItem._id);
					$scope.budgetValueToday.push(responseItem.total);
				});
				//console.log(response);
				console.log($scope.budgetLabelToday);
				console.log($scope.budgetValueToday);

				$scope.budgetObjToday = {};

				for (var i = 0; i < $scope.budgetLabelToday.length; i++){
					$scope.budgetObjToday[$scope.budgetLabelToday[i]] = $scope.budgetValueToday[i];
				}

				console.log($scope.budgetObjToday);
			});
		};


		Projects.get({
			projectId: $stateParams.projectId
		}, function(projectTotalResponse){

			var inputObj = projectTotalResponse.total;
			var labels = Object.keys(projectTotalResponse.total);
			var totalArray = [];
			var data = [];
			$scope.totals = projectTotalResponse.total;

			for(var key in inputObj){
				data.push(inputObj[key]);
				var tempObj = {};
				tempObj[key] = inputObj[key];
				totalArray.push(tempObj);
			}

			$scope.labels = labels;
			$scope.data = data;



		});

		// do the budget calculations here
		// first get the tickets in this project
		// then check which type of issue it is
		// get the hours and multiply with the cost per hour
		// substract the results from the total
		// save the data into the project item

		//ProjectIssues.query(function(projectIssuesResponse){
		//	angular.forEach(projectIssuesResponse, function(item){
		//		if(item.project._id === $stateParams.projectId){
		//			console.log(item);
		//
		//		}
		//	});
		//});

			Budgets.query(function(budgetsResponse){
				var cost;
				angular.forEach(budgetsResponse, function(budgetsItem){
					if(budgetsItem.projectid._id === $stateParams.projectId){
						//console.log(budgetsItem);

						Projects.get({
							projectId: $stateParams.projectId
						}, function(projectResponse){

							var inputObj = projectResponse.total;
							var labels = Object.keys(projectResponse.total);
							var totalArray = [];
							var data = [];
							$scope.totals = projectResponse.total;

							for(var key in inputObj){
								data.push(inputObj[key]);
								var tempObj = {};
								tempObj[key] = inputObj[key];
								totalArray.push(tempObj);
							}

							var l = [];

							angular.forEach(labels, function(labelItem){

								//console.log(labelItem);
								if(labelItem === budgetsItem.issueType.toLowerCase()){

									if(budgetsItem.issueType.toLowerCase() === 'design'){
										console.log(budgetsItem.issueType.toLowerCase());
										angular.forEach(budgetsItem.issueType.toLowerCase(), function(){
											var total = 0;
											total = total + budgetsItem.cost;
											console.log(total);
										});
									}

								}
							});

						});

						//cost = (function(){
						//	// check where the item belongs to in the
						//	switch(budgetsItem.issueType){
						//		case "Design":
						//				var designCost = 0;
						//				return designCost = designCost + budgetsItem.cost;
						//				console.log(designCost);
						//				console.log("cost");
						//			break;
						//		//case "Analysis":
						//		//	return logResponse.time * item.project.charge.analysis;
						//		//	break;
						//		//case "Coding":
						//		//	return logResponse.time * item.project.charge.coding;
						//		//	break;
						//		//case "Documentation":
						//		//	return logResponse.time * item.project.charge.documentation;
						//		//	break;
						//		//case "Bug":
						//		//	return logResponse.time * item.project.charge.coding;
						//		//	break;
						//	}
                        //
						//});

					}
				});

			});


	}
]);

'use strict';

angular.module('projects').controller('ProjectCostController', ['$scope', 'Projects', '$stateParams', '$location', 'ProjectCost', 'Boards',
	function($scope, Projects, $stateParams, $location, ProjectCost, Boards) {

		$scope.analysisTotal = $scope.analysisHours * $scope.analysisCharge;

		$scope.analysisCost = function(){
			$scope.analysisTotal = $scope.analysisHours * $scope.analysisCharge;
		};

		$scope.designCost = function(){
			$scope.designTotal = $scope.designHours * $scope.designCharge;
		};

		$scope.codingCost = function(){
			$scope.codingTotal = $scope.codingHours * $scope.codingCharge;
		};

		$scope.testingCost = function(){
			$scope.testingTotal = $scope.testingHours * $scope.testingCharge;
		};

		$scope.documentationCost = function(){
			$scope.documentationTotal = $scope.documentationHours * $scope.documentationCharge;
		};


		// Update existing Project
		$scope.update = function() {
			var project = $scope.project;

			project.total.analysis = $scope.analysisTotal;
			project.total.design = $scope.designTotal;
			project.total.coding = $scope.codingTotal;
			project.total.testing = $scope.testingTotal;
			project.total.documentation = $scope.documentationTotal;

			project.hours.analysis = $scope.analysisHours;
			project.hours.design = $scope.designHours;
			project.hours.coding = $scope.codingHours;
			project.hours.testing = $scope.testingHours;
			project.hours.documentation = $scope.documentationHours;

			project.charge.analysis = $scope.analysisCharge;
			project.charge.design = $scope.designCharge;
			project.charge.coding = $scope.codingCharge;
			project.charge.testing = $scope.testingCharge;
			project.charge.documentation = $scope.documentationCharge;


			project.$update(function(response) {

				Boards.query(function(boardsResponse){
						angular.forEach(boardsResponse, function(boardsItem){
							if(boardsItem.project._id === response._id){
								$location.path('boards/' + boardsItem._id);
							}
						});
				});

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
			};

				$scope.project = Projects.get({
					projectId: $stateParams.projectId
				});

			//console.log($scope.project);
	}
]);

'use strict';

angular.module('projects').controller('ProjectIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify) {

		$scope.projectArray = [];

		$scope.sortType = 'name'; // set the default sort type
		$scope.sortReverse = false; // set the default sort order
		$scope.searchIssues = ''; // set the default search/filter term

		$scope.project = Projects.get({
			projectId: $stateParams.projectId
		});

		$scope.projectIssues = ProjectIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.project._id === $stateParams.projectId){
					$scope.projectIssue = item;
					$scope.projectArray.push(item);
				}
			});
		});


	}
]);

'use strict';

var projectsApp = angular.module('projects');

projectsApp.controller('ProjectsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Projects', 'UsersList',
	function($scope, $stateParams, $location, Authentication, Projects, UsersList) {

		$scope.searchProjects = '';
		$scope.authentication = Authentication;

		this.projects = Projects.query({company: $scope.authentication.user.userCompany});
	}
]);

//================================================
//Project Create Controller
//================================================

projectsApp.controller('ProjectsCreateController', ['$scope', '$location', 'UsersList', 'Projects', 'Boards', 'Authentication',
	function($scope, $location, UsersList, Projects, Boards, Authentication) {

		$scope.authentication = Authentication;

		console.log($scope.authentication.user.roles);

		//console.log($scope.authentication.user.company);

		// Create new Project
		$scope.create = function() {
			// Create new Project object
			var project = new Projects ({
				name: this.name,
				lead: this.lead,
				dueDate: this.dueDate,
				hours: this.hours,
				chargePerHour: this.chargePerHour,
				cost: this.cost,
				company: $scope.authentication.user.company
			});

			console.log(project);


			// Redirect after save
			project.$save(function(response) {
				//$location.path('projects/' + response._id);

				$location.path('project-cost/' + response._id);

				// create a board with 3 columns
				var board = new Boards({
					name: $scope.name,
					project: response._id,
					company: response.company,
					column: [{name: 'To Do'}, {name: 'In Progress'}, {name: 'Done'}]

				});

				board.$save(function(response){

				}, function(errorResponse){
					$scope.error = errorResponse.data.message;
				});

				// Clear form fields
				$scope.name = '';
				$scope.lead = '';
				$scope.dueDate = '';
				$scope.hours = '';
				$scope.chargePerHour = '';
				$scope.cost = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// fill the key Property
		$scope.fill_key = function(){
			var matches = $scope.name.match(/\b(\w)/g);
			$scope.key = matches.join('');
		};

		$scope.calculate_cost = function(){
			$scope.cost = $scope.hours * $scope.chargePerHour;
		};

		$scope.users_list = [];

		UsersList.query(function(usersResponse){
			angular.forEach(usersResponse, function(item){
				if(item.roles === 'admin' || item.roles === 'manager'){
					$scope.users_list.push(item);
				}
			});
			console.log($scope.users_list);
		});
	}
]);

projectsApp.controller('ProjectsEditController', ['$scope', 'UsersList', '$stateParams', 'Projects', '$location', '$filter',
	function($scope, UsersList, $stateParams, Projects, $location, $filter) {

		$scope.analysisCost = function(){
			$scope.project.total.analysis = $scope.project.hours.analysis * $scope.project.charge.analysis;
		};

		$scope.designCost = function(){
			$scope.project.total.design = $scope.project.hours.design * $scope.project.charge.design;
		};

		$scope.codingCost = function(){
			$scope.project.total.coding= $scope.project.hours.coding * $scope.project.charge.coding;
		};

		$scope.testingCost = function(){
			$scope.project.total.testing = $scope.project.hours.testing * $scope.project.charge.testing;
		};

		$scope.documentationCost = function(){
			$scope.project.total.documentation = $scope.project.hours.documentation * $scope.project.charge.documentation;
		};

		// Update existing Project
		$scope.update = function() {
			var project = $scope.project;

			project.$update(function() {
				$location.path('projects/' + project._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			$scope.project = Projects.get({
				projectId: $stateParams.projectId
			});
		};

		// fill the key Property
		$scope.fill_key = function(){
			var matches = $scope.project.name.match(/\b(\w)/g);
			$scope.project.key = matches.join('');
		};

		$scope.calculate_cost = function(){
			$scope.project.cost = $scope.project.hours * $scope.project.chargePerHour;
		};

		$scope.users_list = [];

		UsersList.query(function(usersResponse){
			angular.forEach(usersResponse, function(item){
				if(item.roles === 'admin' || item.roles === 'manager'){
					$scope.users_list.push(item);
				}
			});
			console.log($scope.users_list);
		});

		$scope.closeProject = function(){
			var closeProject = Projects.get({
				projectId: $stateParams.projectId
			}, function(projectResponse){
				projectResponse.status = 'Closed';
				closeProject.$update(function(updateResponse){
					$scope.success = 'Project Closed';
				}, function(errorResponse){
					$scope.error = errorResponse.message.data;
				})
			})
		}


	}
]);

projectsApp.controller('ProjectsViewController', ['$scope', 'UsersList', '$stateParams', 'Projects', 'Authentication', '$location', 'Users', 'Boards', 'ProjectIssueAgg', 'ProjectIssues', 'AssigneeIssueAgg', 'BudgetAgg',
	function($scope, UsersList, $stateParams, Projects, Authentication, $location, Users, Boards, ProjectIssueAgg, ProjectIssues, AssigneeIssueAgg, BudgetAgg) {

		$scope.authentication = Authentication;

		$scope.findOne = function() {
			$scope.project = Projects.get({
				projectId: $stateParams.projectId
			});
		};

		$scope.goToBoard = function(){
			Boards.query(function(boardsResponse){
				angular.forEach(boardsResponse, function(boardItem){
					if(boardItem.project._id === $stateParams.projectId){
						$location.path('boards/' + boardItem._id);
					}
				});
			});
		};


		this.project = Projects.get({
			projectId: $stateParams.projectId
		});

		this.project.$promise.then(function(data){
			$scope.leadId = data.lead;
			//console.log($scope.leadId);
			//find the project lead
			$scope.findLead = UsersList.get({
				userId: $scope.leadId._id
			});
			//console.log($scope.findLead);
		});

		// Remove existing Project
		$scope.remove = function(project) {
			if ( project ) {
				project.$remove();

				for (var i in $scope.projects) {
					if ($scope.projects [i] === project) {
						$scope.projects.splice(i, 1);
					}
				}
			} else {
				$scope.project.$remove(function() {
					$location.path('projects');
				});
			}
		};

		Projects.get({
			projectId: $stateParams.projectId
		}, function(projectTotalResponse){

			var inputObj = projectTotalResponse.total;
			var labels = Object.keys(projectTotalResponse.total);
			var data = [];
			$scope.hours = projectTotalResponse.hours;
			$scope.totals = projectTotalResponse.total;

			for(var key in inputObj){
				data.push(inputObj[key]);
			}

			$scope.labels = labels;
			$scope.data = data;

			//console.log($scope.labels);
			//console.log($scope.data);

		});



		$scope.openIssues = [];
		$scope.closedIssues = [];

		ProjectIssues.query(function(projectIssueResponse){
			angular.forEach(projectIssueResponse, function(projectIssueItem){
				if(projectIssueItem.project._id === $stateParams.projectId){
					if(projectIssueItem.issue.status === 'Open'){
						$scope.openIssues.push(projectIssueItem.issue);
					} else {
						$scope.closedIssues.push(projectIssueItem.issue);
					}
				}
			});
			//console.log($scope.openIssues);
			//console.log($scope.closedIssues);

			$scope.progressLabels = ['Issues In Progress', 'Completed Issues'];
			$scope.progressData = [$scope.openIssues.length, $scope.closedIssues.length];

		});

		AssigneeIssueAgg.query(function(assigneeIssueResponse){
			angular.forEach(assigneeIssueResponse, function(assigneeIssueItem){
				//console.log(assigneeIssueItem);
			});
		});

		BudgetAgg.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabel = [];
			$scope.budgetValue = [];
			$scope.hours = [];
			angular.forEach(response, function(responseItem){
				$scope.budgetLabel.push(responseItem._id);
				$scope.budgetValue.push(responseItem.total);
				$scope.hours.push(responseItem.hours);
				console.log(responseItem);
			});

			console.log($scope.hours)
			//console.log(response);

			$scope.budgetObj = {};
			$scope.budgetHoursObj = {};

			for (var i = 0; i < $scope.budgetLabel.length; i++){
				$scope.budgetObj[$scope.budgetLabel[i]] = $scope.budgetValue[i];
			}

			for (var i = 0; i < $scope.budgetLabel.length; i++){
				$scope.budgetHoursObj[$scope.budgetLabel[i]] = $scope.hours[i];
			}

		});

	}
]);






'use strict';

angular.module('projects').controller('WorkLoggedController', ['$scope', 'WorkLogged', '$stateParams', 'Projects', 'WorkLoggedToday', 'WorkLoggedWeek', 'WorkLoggedMonth', 'WorkLoggedDay',
	function($scope, WorkLogged, $stateParams, Projects, WorkLoggedToday, WorkLoggedWeek, WorkLoggedMonth, WorkLoggedDay) {

		$scope.loggedWork = [];

		WorkLogged.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWork.push(item);
			});
		});

		$scope.loggedWorkToday = [];
		WorkLoggedToday.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWorkToday.push(item);
			});
		});

		$scope.loggedWorkWeek = [];
		WorkLoggedWeek.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWorkWeek.push(item);
			});
		});

		$scope.loggedWorkMonth = [];
		WorkLoggedMonth.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWorkMonth.push(item);
			});
		});

		$scope.today = function() {
			$scope.dt = new Date();
		};

		//$scope.today();
		$scope.dt = new Date();

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		$scope.disabled = function(date, mode) {
			return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.toggleMin = function() {
			$scope.minDate = $scope.minDate ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];

		$scope.loggedWorkDay = [];
		$scope.getWork = function(dt){
			WorkLoggedDay.query({
				projectId: $stateParams.projectId,
				date: dt
			},function(response){
				//console.log(response);
				angular.forEach(response, function(item){
					console.log(item);
					$scope.loggedWorkDay.push(item);
				});
			});
		}


		$scope.project  = Projects.get({
			projectId: $stateParams.projectId
		});

	}
]);

'use strict';

angular.module('projects').directive('projects', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Projects directive logic
				// ...

				element.text('this is the projects directive');
			}
		};
	}
]).directive('myDate', ['$timeout', '$filter', function ($timeout, $filter)
{
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModelController) {
			ngModelController.$parsers.push(function(data) {
				//View -> Model
				return data;
			});
			ngModelController.$formatters.push(function(data) {
				//Model -> View
				return $filter('date')(data, 'yyyy-MM-dd');
			});
		}
	};
}]);

'use strict';

angular.module('projects').filter('projectKey', [
	function() {
		return function(input) {

			var key = '';

			for(var i = 0; i < input.length; i++)

			return 'projects filter: ' + input;
		};
	}
]);

'use strict';

//Projects service used to communicate Projects REST endpoints
angular.module('projects').factory('Projects', ['$resource',
	function($resource) {
		return $resource('projects/:projectId', { projectId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('ProjectCost', ['$resource',
	function($resource) {
		return $resource('project-cost/:projectId', { projectId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Budgets', ['$resource',
	function($resource) {
		return $resource('budgets/:budgetId', { budgetId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('BudgetAgg', ['$resource',
	function($resource) {
		return $resource('budgetAgg/:budgetAggId/:date', {budgetAggId: '@_id'}, {date: 'date'},
			{
			
			});
	}
]).factory('ProjectIssueAgg', ['$resource',
	function($resource) {
		return $resource('projectIssueAgg/:projectId/:date', {projectId: '@_id'}, {date: 'date'},
			{

			});
	}
]).factory('AssigneeIssueAgg', ['$resource',
	function($resource) {
		return $resource('assigneeIssueAgg/', {},
			{

			});
	}
]).factory('WorkLogged', ['$resource',
	function($resource) {
		return $resource('logged/:projectId/:date', {projectId: '@_id'}, {date: 'date'},
			{

			});
	}
]).factory('BudgetWeek', ['$resource',
	function($resource) {
		return $resource('budgetWeek/:budgetAggId', { budgetAggId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('BudgetMonth', ['$resource',
		function($resource) {
			return $resource('budgetMonth/:budgetAggId', { budgetAggId: '@_id'
			}, {
				update: {
					method: 'PUT'
				}
			});
		}
]).factory('BudgetAggDate', ['$resource',
	function($resource) {
		return $resource('budgetAgg/:budgetAggId/:date', {budgetAggId: '@_id'}, {date: 'date'},
			{

			});
	}
]).factory('WorkLoggedToday', ['$resource',
	function($resource) {
		return $resource('loggedToday/:projectId', {projectId: '@_id'},
			{

			});
	}
]).factory('WorkLoggedWeek', ['$resource',
	function($resource) {
		return $resource('loggedWeek/:projectId', {projectId: '@_id'},
			{

			});
	}
]).factory('WorkLoggedMonth', ['$resource',
	function($resource) {
		return $resource('loggedMonth/:projectId', {projectId: '@_id'},
			{

			});
	}
]).factory('WorkLoggedDay', ['$resource',
	function($resource) {
		return $resource('loggedDay/:projectId/:date', {projectId: '@_id'}, {date: 'date'},
			{

			});
	}
]);

'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {


		// Users state routing
		$stateProvider.
		//state('site', {
		//	url: '/site',
		//	abstract: true,
		//	template: '<ui-view/>'
		//}).
		state('view-user', {
			url:'/list-user/:userId',
			templateUrl: 'modules/users/views/view-user.client.view.html'
		}).
		state('list-user', {
			url: '/list-user',
			templateUrl: 'modules/users/views/list-user.client.view.html'
		}).
		state('create-user', {
			url: '/create-user',
			templateUrl: 'modules/users/views/create-user.client.view.html'
		}).
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);

'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', 'Users', 'UsersList',
	function($scope, $http, $location, Authentication, Users, UsersList) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function(isValid) {

			console.log($scope.credentials);

			//$scope.credentials.roles = 'admin';
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the dashboard of the company
				$location.path('/');

				//// Create new Company object
				//var company = new Companies ({
				//	name: $scope.companyName
				//});
                //
				////save company object
				//company.$save(function(response) {
				//	//$location.path('companies/' + response._id);
                //
				//	// Clear form fields
				//	//$scope.name = '';
                //
				//	var user =  UsersList.get({
				//		userId: $scope.authentication.user._id
                //
				//	}, function(user){
				//		user.userCompany = response._id;
				//		//console.log(response._id);
				//		user.$update(function(data){
				//		}, function(res){
				//			$scope.error = res.data.message;
				//		});
				//	});
                //
				//}, function(errorResponse) {
				//	$scope.error = errorResponse.data.message;
				//}); // end of create company

			}).error(function(response) {
				$scope.error = response.message;
			});
		}; //end of create user

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);

'use strict';
var createUserController = angular.module('users');

createUserController.controller('CreateUserController', ['$scope', '$http', '$location', 'Authentication', 'Users', 'UsersList',
	function($scope, $http, $location, Authentication, Users, UsersList) {

		$scope.authentication = Authentication;
		$scope.rolesArray = ['manager', 'member', 'viewer'];

		console.log($scope.authentication.user.company);

		// If user is signed in then redirect back home
		//if ($scope.authentication.user) $location.path('/');



		$scope.create = function() {

			$scope.credentials.company = $scope.authentication.user.company;
			console.log($scope.credentials);

			$http.post('/auth/create', $scope.credentials).success(function(response) {


				// And reload page
				$location.path('/create-user');
				//console.log(response.message);
				$scope.success = 'User Created Successfully';

				//clear form fields
				$scope.credentials.firstName = '';
				$scope.credentials.lastName = '';
				$scope.credentials.email = '';
				$scope.credentials.username = '';
				$scope.credentials.password = '';

			}).error(function(response) {
				$scope.error = response.message;
			});

		};


	}
]);

'use strict';

angular.module('users').controller('ListUserController', ['$scope', '$http', '$location', 'UsersList', 'Authentication',
	function($scope, $http, $location, UsersList, Authentication) {

		$scope.find = function() {
			$scope.list_users = UsersList.query();
		};

		$scope.show = function(id){
			$location.url('/list-user/' + id);
		};


	}
]);

'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication',
	function($scope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('ViewUserController', ['$scope', 'UsersList', '$stateParams', 'Authentication',
	function($scope, UsersList, $stateParams ,Authentication) {
		//$scope.user = Authentication.user;

		$scope.deactivateUser = function(){
			var user= UsersList.get({
				userId: $stateParams.userId
			}, function(response){
			response.status = 'inactive';

				user.$update(function(updateResponse){
					$scope.success = 'User Deactivated';
				}, function(errorResponse){
					$scope.error = errorResponse.message.data;
				});
		});
		};

		$scope.findOne = function() {
			$scope.view_user = UsersList.get({
				userId: $stateParams.userId
			});
		};

		console.log(UsersList.get({
			userId: $stateParams.userId
		}));
	}
]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users/:userId', {userId: '@id'}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('UsersList', ['$resource',
	function($resource) {
		return $resource('list-user/:userId', {userId: '@id'}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
