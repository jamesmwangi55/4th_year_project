'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users/:userId', {userId: '@id'}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('UsersList', ['$resource',
	function($resource) {
		return $resource('list-user/:userId', {userId: '@id'}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
