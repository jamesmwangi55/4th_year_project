'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', 'Users', 'UsersList',
	function($scope, $http, $location, Authentication, Users, UsersList) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function(isValid) {

			console.log($scope.credentials);

			//$scope.credentials.roles = 'admin';
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the dashboard of the company
				$location.path('/');

				//// Create new Company object
				//var company = new Companies ({
				//	name: $scope.companyName
				//});
                //
				////save company object
				//company.$save(function(response) {
				//	//$location.path('companies/' + response._id);
                //
				//	// Clear form fields
				//	//$scope.name = '';
                //
				//	var user =  UsersList.get({
				//		userId: $scope.authentication.user._id
                //
				//	}, function(user){
				//		user.userCompany = response._id;
				//		//console.log(response._id);
				//		user.$update(function(data){
				//		}, function(res){
				//			$scope.error = res.data.message;
				//		});
				//	});
                //
				//}, function(errorResponse) {
				//	$scope.error = errorResponse.data.message;
				//}); // end of create company

			}).error(function(response) {
				$scope.error = response.message;
			});
		}; //end of create user

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
