'use strict';

angular.module('users').controller('ViewUserController', ['$scope', 'UsersList', '$stateParams', 'Authentication',
	function($scope, UsersList, $stateParams ,Authentication) {
		//$scope.user = Authentication.user;

		$scope.deactivateUser = function(){
			var user= UsersList.get({
				userId: $stateParams.userId
			}, function(response){
			response.status = 'inactive';

				user.$update(function(updateResponse){
					$scope.success = 'User Deactivated';
				}, function(errorResponse){
					$scope.error = errorResponse.message.data;
				});
		});
		};

		$scope.findOne = function() {
			$scope.view_user = UsersList.get({
				userId: $stateParams.userId
			});
		};

		console.log(UsersList.get({
			userId: $stateParams.userId
		}));
	}
]);
