'use strict';
var createUserController = angular.module('users');

createUserController.controller('CreateUserController', ['$scope', '$http', '$location', 'Authentication', 'Users', 'UsersList',
	function($scope, $http, $location, Authentication, Users, UsersList) {

		$scope.authentication = Authentication;
		$scope.rolesArray = ['manager', 'member', 'viewer'];

		console.log($scope.authentication.user.company);

		// If user is signed in then redirect back home
		//if ($scope.authentication.user) $location.path('/');



		$scope.create = function() {

			$scope.credentials.company = $scope.authentication.user.company;
			console.log($scope.credentials);

			$http.post('/auth/create', $scope.credentials).success(function(response) {


				// And reload page
				$location.path('/create-user');
				//console.log(response.message);
				$scope.success = 'User Created Successfully';

				//clear form fields
				$scope.credentials.firstName = '';
				$scope.credentials.lastName = '';
				$scope.credentials.email = '';
				$scope.credentials.username = '';
				$scope.credentials.password = '';

			}).error(function(response) {
				$scope.error = response.message;
			});

		};


	}
]);
