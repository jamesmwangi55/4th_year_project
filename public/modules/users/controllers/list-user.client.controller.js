'use strict';

angular.module('users').controller('ListUserController', ['$scope', '$http', '$location', 'UsersList', 'Authentication',
	function($scope, $http, $location, UsersList, Authentication) {

		$scope.find = function() {
			$scope.list_users = UsersList.query();
		};

		$scope.show = function(id){
			$location.url('/list-user/' + id);
		};


	}
]);
