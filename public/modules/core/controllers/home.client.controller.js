'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication', 'AssigneeIssues',
	function($scope, Authentication, AssigneeIssues) {

		$scope.authentication = Authentication;

		$scope.issues = [];

		AssigneeIssues.query(function (response) {
			angular.forEach(response, function (item) {
				//if(item.assignee == null){
				//	continue;
				//}
				if (item.assignee._id === $scope.authentication.user._id) {
					$scope.issues.push(item.issue);
				}
			});
		});

		$scope.query = {};

		$scope.type = {
			'design': 'Design',
			'analysis': 'Analysis',
			'documentation': 'Documentation',
			'testing': 'Testing',
			'coding': 'Coding'
		};

		$scope.checkBoxes = [
			{"type": "Analysis", checked: false},
			{"type": "Documentation", checked: false},
			{"type": "Coding", checked: false},
			{"type": "Testing", checked: false},
			{"type": "Design", checked: false}
		]
	}
]);

