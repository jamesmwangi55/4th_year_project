'use strict';

//Setting up route
angular.module('boards').config(['$stateProvider',
	function($stateProvider) {
		// Boards state routing
		$stateProvider.
		state('delete-column', {
			url: '/delete-column',
			templateUrl: 'modules/boards/views/delete-column.client.view.html'
		}).
		state('column-name', {
			url: '/column-name',
			templateUrl: 'modules/boards/views/column-name.client.view.html'
		}).
		state('create-column', {
			url: '/create-column',
			templateUrl: 'modules/boards/views/create-column.client.view.html'
		}).
		state('ticket', {
			url: '/ticket',
			templateUrl: 'modules/boards/views/ticket.client.view.html'
		}).
		state('listBoards', {
			url: '/boards',
			templateUrl: 'modules/boards/views/list-boards.client.view.html'
		}).
		state('createBoard', {
			url: '/boards/create',
			templateUrl: 'modules/boards/views/create-board.client.view.html'
		}).
		state('viewBoard', {
			url: '/boards/:boardId',
			templateUrl: 'modules/boards/views/view-board.client.view.html'
		}).
		state('editBoard', {
			url: '/boards/:boardId/edit',
			templateUrl: 'modules/boards/views/edit-board.client.view.html'
		});
	}
]);