'use strict';

var boardApp = angular.module('boards');

//==============================================================
// List Boards Controller
//==============================================================

boardApp.controller('BoardsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards',
	function($scope, $stateParams, $location, Authentication, Boards) {
		$scope.authentication = Authentication;

		// Find a list of Boards
		$scope.find = function() {
			$scope.boards = Boards.query();
		};

	}
]);

//==============================================================
// Create Board Controller
//==============================================================

boardApp.controller('BoardsCreateController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards',
	function($scope, $stateParams, $location, Authentication, Boards) {
		$scope.authentication = Authentication;

		$scope.handleDrop = function() {
			alert('Item has been dropped');
		};

		// Create new Board
		$scope.create = function() {
			// Create new Board object
			var board = new Boards ({
				name: this.name
			});

			// Redirect after save
			board.$save(function(response) {
				$location.path('boards/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

	}
]); // end of create board controller


//==============================================================
// Edit Board Controller
//==============================================================

boardApp.controller('BoardsEditController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards',
	function($scope, $stateParams, $location, Authentication, Boards) {
		$scope.authentication = Authentication;

		// Update existing Board
		$scope.update = function() {
			var board = $scope.board;

			board.$update(function() {
				$location.path('boards/' + board._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find existing Board
		$scope.findOne = function() {
			$scope.board = Boards.get({
				boardId: $stateParams.boardId
			});
		};

	}
]); // end of edit board controller

//==============================================================
// View Board Controller
//==============================================================

boardApp.controller('BoardsViewController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify', '$modal', '$log', '$window', '$state',
	function($scope, $stateParams, $location, Authentication, Boards, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify, $modal, $log, $window, $state) {

		$scope.authentication = Authentication;
		$scope.projectIssueArray = [];
		$scope.currentIssue;
		$scope.currentColumn;
		$scope.columnName;

		$scope.goToProject = function(){

			Boards.get({
				boardId: $stateParams.boardId
			}, function(boardResponse){
				Projects.query(function(projectsResponse){
					angular.forEach(projectsResponse, function(projectItem){
						if(projectItem._id === boardResponse.project._id){
							$location.path('projects/' + projectItem._id);
						}
					});
				});
			});

		};




// Remove existing Board
		$scope.remove = function(board) {
			if ( board ) {
				board.$remove();

				for (var i in $scope.boards) {
					if ($scope.boards [i] === board) {
						$scope.boards.splice(i, 1);
					}
				}
			} else {
				$scope.board.$remove(function() {
					$location.path('boards');
				});
			}
		};


		// Find existing Board
		$scope.findOne = function() {
			$scope.board = Boards.get({
				boardId: $stateParams.boardId
			});
		};

		// get a list of columns in the board
		$scope.getColumns = Boards.get({
			boardId: $stateParams.boardId
		}, function(response){
			$scope.columns = response.column;
		});

		// get a list of issues that belong to this project
		$scope.getIssues = Boards.get({
			boardId: $stateParams.boardId
		}, function(boardResponse){

			//$scope.projectIssues = ProjectIssues.query(function(response){
			//	angular.forEach(response, function(item){
            //
			//		if(item.project._id === boardResponse.project._id){
            //
			//			$scope.projectIssue = item;
			//			$scope.projectIssueArray.push(item);
			//		}
			//	});
            //
			//});

			angular.forEach(boardResponse.column, function(item){
				angular.forEach(item.issues, function(item2){

					$scope.projectIssueArray.push(item2);

				});

				//display each issue in column where it belongs

				//console.log($scope.projectIssueArray);
				//console.log(item.issues);
				$scope.projectIssue = item;

			});

		});

		$scope.getCurrentIssue = function(issue){
			$scope.currentIssue = issue;
			//console.log($scope.currentIssue);
		};

		$scope.getCurrentColumn = function(column){
			$scope.currentColumn = column;
			//console.log($scope.currentColumn);
		};

		//move item from one array to another when dropped
		$scope.handleDrop = function() {

			var board = Boards.get({
				boardId: $stateParams.boardId
			}, function(boardResponse){

				//delete item from column
				//console.log(boardResponse.column);

				angular.forEach(boardResponse.column, function(item){
					angular.forEach(item.issues, function(item2){
						if(item2._id === $scope.currentIssue._id){
							//console.log(item.issues);

							var index = item.issues.indexOf(item2);
							//console.log(index);

							//save item in new variable
							var savedItem = item2;

							//delete item form array
							item.issues.splice(index, 1); // uncomment once the code to move to new column is ready

							//add item to new column
							angular.forEach(boardResponse.column, function(item3){
								//console.log(item3);
								if(item3._id === $scope.currentColumn._id ){
									//console.log("the column to push data to is");
									//console.log(item3.issues);
									var columnIndex = boardResponse.column.indexOf(item3);

									if(boardResponse.column[columnIndex].issues.indexOf($scope.currentIssue._id) === -1){
										boardResponse.column[columnIndex].issues.push($scope.currentIssue._id);
										//console.log("Item Moved")
									}

									Array.prototype.last = function () {
										return this[this.length - 1];
									};

									if(item3 === boardResponse.column.last()){
										var issue = Issues.get({
											issueId: $scope.currentIssue._id
										}, function(issueResponse){
											//console.log(issueResponse);
											issueResponse.status  = 'Closed';

											issue.$update(function(updatedResponse){

											}, function(errorResponse){
												$scope.error = errorResponse.data.message;
											});
										});
									}

									//console.log(boardResponse.column.indexOf(item3));

									//item3.issues.push();
								}
							});
						}

					});

				});

				//boardResponse.column[0].issues.push(response._id)
				//console.log(boardResponse.column[0].issues);

				board.$update(function(updateResponse){
					//console.log(boardResponse);


					// reload the page
					//$location.path('boards/' + boardResponse._id).reload();
					//$window.location.reload()
					//$route.reload();
					$state.reload();

				}, function(errorResponse){
					$scope.error = errorResponse.data.message
				});
			});



		};


		// Open a modal window to add a column
		$scope.newColumn = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/boards/views/create-column.client.view.html',
				controller: function ($scope, $modalInstance) {

					$scope.ok = function (columnName) {

						var board = Boards.get({
							boardId: $stateParams.boardId
						}, function(boardResponse){
							console.log(boardResponse.column);

							console.log(columnName);

							boardResponse.column.splice(boardResponse.column.length - 1, 0 , {name: columnName});

							// add column
							board.$update(function(response){
								$state.reload();
							}, function(errorResponse){
								$scope.error = errorResponse.data.message;
							})
						});

						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				},
				size: size,
				resolve: {
					items: function () {
						return $scope.columnName;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		// open changeName modal window
		$scope.changeName = function (size, column) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/boards/views/column-name.client.view.html',
				controller: function($scope, $modalInstance){

					$scope.column = column;

					$scope.ok = function () {

						console.log(column);

						var board = Boards.get({
							boardId: $stateParams.boardId
						}, function(boardResponse){

							angular.forEach(boardResponse.column, function(item){
								if(item._id === column._id){

									item.name = column.name;

									board.$update(function(response){
										console.log(response);
									}, function(errorResponse){
										$scope.error = errorResponse.data.message;
									});
								}
							});


						});


						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				},
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		// Delete current column modal window
		$scope.deleteColumn = function (size, column) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/boards/views/delete-column.client.view.html',
				controller: function($scope, $modalInstance){

					$scope.column = column;

					$scope.ok = function () {


						var board = Boards.get({
							boardId: $stateParams.boardId
						}, function(boardResponse){

							angular.forEach(boardResponse.column, function(item){
								if(item._id === column._id){

									$scope.state = false;

									console.log(item.issues.length);

									if(item.issues.length === 0){
										boardResponse.column.splice(boardResponse.column.indexOf(item), 1);
										//console.log(boardResponse.column.indexOf(item));
										//console.log(item._id);
									} else {
										$scope.state = true;
									}

									board.$update(function(response){
										//console.log(response);
										$state.reload();
									}, function(errorResponse){
										$scope.error = errorResponse.data.message;
									});
								}
							});


						});


						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				},
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

	}
]); // End of view board controller


boardApp.controller('ItemController', ['$scope', '$stateParams', '$location', 'Authentication', 'Boards', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify',
	function($scope, $stateParams, $location, Authentication, Boards, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify) {

	}
]);
