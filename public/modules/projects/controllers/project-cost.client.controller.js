'use strict';

angular.module('projects').controller('ProjectCostController', ['$scope', 'Projects', '$stateParams', '$location', 'ProjectCost', 'Boards',
	function($scope, Projects, $stateParams, $location, ProjectCost, Boards) {

		$scope.analysisTotal = $scope.analysisHours * $scope.analysisCharge;

		$scope.analysisCost = function(){
			$scope.analysisTotal = $scope.analysisHours * $scope.analysisCharge;
		};

		$scope.designCost = function(){
			$scope.designTotal = $scope.designHours * $scope.designCharge;
		};

		$scope.codingCost = function(){
			$scope.codingTotal = $scope.codingHours * $scope.codingCharge;
		};

		$scope.testingCost = function(){
			$scope.testingTotal = $scope.testingHours * $scope.testingCharge;
		};

		$scope.documentationCost = function(){
			$scope.documentationTotal = $scope.documentationHours * $scope.documentationCharge;
		};


		// Update existing Project
		$scope.update = function() {
			var project = $scope.project;

			project.total.analysis = $scope.analysisTotal;
			project.total.design = $scope.designTotal;
			project.total.coding = $scope.codingTotal;
			project.total.testing = $scope.testingTotal;
			project.total.documentation = $scope.documentationTotal;

			project.hours.analysis = $scope.analysisHours;
			project.hours.design = $scope.designHours;
			project.hours.coding = $scope.codingHours;
			project.hours.testing = $scope.testingHours;
			project.hours.documentation = $scope.documentationHours;

			project.charge.analysis = $scope.analysisCharge;
			project.charge.design = $scope.designCharge;
			project.charge.coding = $scope.codingCharge;
			project.charge.testing = $scope.testingCharge;
			project.charge.documentation = $scope.documentationCharge;


			project.$update(function(response) {

				Boards.query(function(boardsResponse){
						angular.forEach(boardsResponse, function(boardsItem){
							if(boardsItem.project._id === response._id){
								$location.path('boards/' + boardsItem._id);
							}
						});
				});

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
			};

				$scope.project = Projects.get({
					projectId: $stateParams.projectId
				});

			//console.log($scope.project);
	}
]);
