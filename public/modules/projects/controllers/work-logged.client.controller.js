'use strict';

angular.module('projects').controller('WorkLoggedController', ['$scope', 'WorkLogged', '$stateParams', 'Projects', 'WorkLoggedToday', 'WorkLoggedWeek', 'WorkLoggedMonth', 'WorkLoggedDay',
	function($scope, WorkLogged, $stateParams, Projects, WorkLoggedToday, WorkLoggedWeek, WorkLoggedMonth, WorkLoggedDay) {

		$scope.loggedWork = [];

		WorkLogged.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWork.push(item);
			});
		});

		$scope.loggedWorkToday = [];
		WorkLoggedToday.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWorkToday.push(item);
			});
		});

		$scope.loggedWorkWeek = [];
		WorkLoggedWeek.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWorkWeek.push(item);
			});
		});

		$scope.loggedWorkMonth = [];
		WorkLoggedMonth.query({
			projectId: $stateParams.projectId
		},function(response){
			//console.log(response);
			angular.forEach(response, function(item){
				console.log(item);
				$scope.loggedWorkMonth.push(item);
			});
		});

		$scope.today = function() {
			$scope.dt = new Date();
		};

		//$scope.today();
		$scope.dt = new Date();

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		$scope.disabled = function(date, mode) {
			return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.toggleMin = function() {
			$scope.minDate = $scope.minDate ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];

		$scope.loggedWorkDay = [];
		$scope.getWork = function(dt){
			WorkLoggedDay.query({
				projectId: $stateParams.projectId,
				date: dt
			},function(response){
				//console.log(response);
				angular.forEach(response, function(item){
					console.log(item);
					$scope.loggedWorkDay.push(item);
				});
			});
		}


		$scope.project  = Projects.get({
			projectId: $stateParams.projectId
		});

	}
]);
