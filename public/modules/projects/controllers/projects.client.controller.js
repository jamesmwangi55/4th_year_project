'use strict';

var projectsApp = angular.module('projects');

projectsApp.controller('ProjectsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Projects', 'UsersList',
	function($scope, $stateParams, $location, Authentication, Projects, UsersList) {

		$scope.searchProjects = '';
		$scope.authentication = Authentication;

		this.projects = Projects.query({company: $scope.authentication.user.userCompany});
	}
]);

//================================================
//Project Create Controller
//================================================

projectsApp.controller('ProjectsCreateController', ['$scope', '$location', 'UsersList', 'Projects', 'Boards', 'Authentication',
	function($scope, $location, UsersList, Projects, Boards, Authentication) {

		$scope.authentication = Authentication;

		console.log($scope.authentication.user.roles);

		//console.log($scope.authentication.user.company);

		// Create new Project
		$scope.create = function() {
			// Create new Project object
			var project = new Projects ({
				name: this.name,
				lead: this.lead,
				dueDate: this.dueDate,
				hours: this.hours,
				chargePerHour: this.chargePerHour,
				cost: this.cost,
				company: $scope.authentication.user.company
			});

			console.log(project);


			// Redirect after save
			project.$save(function(response) {
				//$location.path('projects/' + response._id);

				$location.path('project-cost/' + response._id);

				// create a board with 3 columns
				var board = new Boards({
					name: $scope.name,
					project: response._id,
					company: response.company,
					column: [{name: 'To Do'}, {name: 'In Progress'}, {name: 'Done'}]

				});

				board.$save(function(response){

				}, function(errorResponse){
					$scope.error = errorResponse.data.message;
				});

				// Clear form fields
				$scope.name = '';
				$scope.lead = '';
				$scope.dueDate = '';
				$scope.hours = '';
				$scope.chargePerHour = '';
				$scope.cost = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// fill the key Property
		$scope.fill_key = function(){
			var matches = $scope.name.match(/\b(\w)/g);
			$scope.key = matches.join('');
		};

		$scope.calculate_cost = function(){
			$scope.cost = $scope.hours * $scope.chargePerHour;
		};

		$scope.users_list = [];

		UsersList.query(function(usersResponse){
			angular.forEach(usersResponse, function(item){
				if(item.roles === 'admin' || item.roles === 'manager'){
					$scope.users_list.push(item);
				}
			});
			console.log($scope.users_list);
		});
	}
]);

projectsApp.controller('ProjectsEditController', ['$scope', 'UsersList', '$stateParams', 'Projects', '$location', '$filter',
	function($scope, UsersList, $stateParams, Projects, $location, $filter) {

		$scope.analysisCost = function(){
			$scope.project.total.analysis = $scope.project.hours.analysis * $scope.project.charge.analysis;
		};

		$scope.designCost = function(){
			$scope.project.total.design = $scope.project.hours.design * $scope.project.charge.design;
		};

		$scope.codingCost = function(){
			$scope.project.total.coding= $scope.project.hours.coding * $scope.project.charge.coding;
		};

		$scope.testingCost = function(){
			$scope.project.total.testing = $scope.project.hours.testing * $scope.project.charge.testing;
		};

		$scope.documentationCost = function(){
			$scope.project.total.documentation = $scope.project.hours.documentation * $scope.project.charge.documentation;
		};

		// Update existing Project
		$scope.update = function() {
			var project = $scope.project;

			project.$update(function() {
				$location.path('projects/' + project._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findOne = function() {
			$scope.project = Projects.get({
				projectId: $stateParams.projectId
			});
		};

		// fill the key Property
		$scope.fill_key = function(){
			var matches = $scope.project.name.match(/\b(\w)/g);
			$scope.project.key = matches.join('');
		};

		$scope.calculate_cost = function(){
			$scope.project.cost = $scope.project.hours * $scope.project.chargePerHour;
		};

		$scope.users_list = [];

		UsersList.query(function(usersResponse){
			angular.forEach(usersResponse, function(item){
				if(item.roles === 'admin' || item.roles === 'manager'){
					$scope.users_list.push(item);
				}
			});
			console.log($scope.users_list);
		});

		$scope.closeProject = function(){
			var closeProject = Projects.get({
				projectId: $stateParams.projectId
			}, function(projectResponse){
				projectResponse.status = 'Closed';
				closeProject.$update(function(updateResponse){
					$scope.success = 'Project Closed';
				}, function(errorResponse){
					$scope.error = errorResponse.message.data;
				})
			})
		}


	}
]);

projectsApp.controller('ProjectsViewController', ['$scope', 'UsersList', '$stateParams', 'Projects', 'Authentication', '$location', 'Users', 'Boards', 'ProjectIssueAgg', 'ProjectIssues', 'AssigneeIssueAgg', 'BudgetAgg',
	function($scope, UsersList, $stateParams, Projects, Authentication, $location, Users, Boards, ProjectIssueAgg, ProjectIssues, AssigneeIssueAgg, BudgetAgg) {

		$scope.authentication = Authentication;

		$scope.findOne = function() {
			$scope.project = Projects.get({
				projectId: $stateParams.projectId
			});
		};

		$scope.goToBoard = function(){
			Boards.query(function(boardsResponse){
				angular.forEach(boardsResponse, function(boardItem){
					if(boardItem.project._id === $stateParams.projectId){
						$location.path('boards/' + boardItem._id);
					}
				});
			});
		};


		this.project = Projects.get({
			projectId: $stateParams.projectId
		});

		this.project.$promise.then(function(data){
			$scope.leadId = data.lead;
			//console.log($scope.leadId);
			//find the project lead
			$scope.findLead = UsersList.get({
				userId: $scope.leadId._id
			});
			//console.log($scope.findLead);
		});

		// Remove existing Project
		$scope.remove = function(project) {
			if ( project ) {
				project.$remove();

				for (var i in $scope.projects) {
					if ($scope.projects [i] === project) {
						$scope.projects.splice(i, 1);
					}
				}
			} else {
				$scope.project.$remove(function() {
					$location.path('projects');
				});
			}
		};

		Projects.get({
			projectId: $stateParams.projectId
		}, function(projectTotalResponse){

			var inputObj = projectTotalResponse.total;
			var labels = Object.keys(projectTotalResponse.total);
			var data = [];
			$scope.hours = projectTotalResponse.hours;
			$scope.totals = projectTotalResponse.total;

			for(var key in inputObj){
				data.push(inputObj[key]);
			}

			$scope.labels = labels;
			$scope.data = data;

			//console.log($scope.labels);
			//console.log($scope.data);

		});



		$scope.openIssues = [];
		$scope.closedIssues = [];

		ProjectIssues.query(function(projectIssueResponse){
			angular.forEach(projectIssueResponse, function(projectIssueItem){
				if(projectIssueItem.project._id === $stateParams.projectId){
					if(projectIssueItem.issue.status === 'Open'){
						$scope.openIssues.push(projectIssueItem.issue);
					} else {
						$scope.closedIssues.push(projectIssueItem.issue);
					}
				}
			});
			//console.log($scope.openIssues);
			//console.log($scope.closedIssues);

			$scope.progressLabels = ['Issues In Progress', 'Completed Issues'];
			$scope.progressData = [$scope.openIssues.length, $scope.closedIssues.length];

		});

		AssigneeIssueAgg.query(function(assigneeIssueResponse){
			angular.forEach(assigneeIssueResponse, function(assigneeIssueItem){
				//console.log(assigneeIssueItem);
			});
		});

		BudgetAgg.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabel = [];
			$scope.budgetValue = [];
			$scope.hours = [];
			angular.forEach(response, function(responseItem){
				$scope.budgetLabel.push(responseItem._id);
				$scope.budgetValue.push(responseItem.total);
				$scope.hours.push(responseItem.hours);
				console.log(responseItem);
			});

			console.log($scope.hours)
			//console.log(response);

			$scope.budgetObj = {};
			$scope.budgetHoursObj = {};

			for (var i = 0; i < $scope.budgetLabel.length; i++){
				$scope.budgetObj[$scope.budgetLabel[i]] = $scope.budgetValue[i];
			}

			for (var i = 0; i < $scope.budgetLabel.length; i++){
				$scope.budgetHoursObj[$scope.budgetLabel[i]] = $scope.hours[i];
			}

		});

	}
]);





