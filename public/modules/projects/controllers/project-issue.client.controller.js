'use strict';

angular.module('projects').controller('ProjectIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify) {

		$scope.projectArray = [];

		$scope.sortType = 'name'; // set the default sort type
		$scope.sortReverse = false; // set the default sort order
		$scope.searchIssues = ''; // set the default search/filter term

		$scope.project = Projects.get({
			projectId: $stateParams.projectId
		});

		$scope.projectIssues = ProjectIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.project._id === $stateParams.projectId){
					$scope.projectIssue = item;
					$scope.projectArray.push(item);
				}
			});
		});


	}
]);
