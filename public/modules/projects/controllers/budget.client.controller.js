'use strict';

angular.module('projects').controller('BudgetController', ['$scope', 'UsersList', '$stateParams', 'Projects', 'Authentication', '$location', 'Users', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify', '$modal', '$log', 'Boards', 'Logs', 'Budgets', 'BudgetAgg', 'BudgetWeek', 'BudgetMonth', 'BudgetAggDate',
	function($scope, UsersList, $stateParams, Projects, Authentication, $location, Users, AssigneeIssues, ProjectIssues, Comments, Notify, $modal, $log, Boards, Logs, Budgets, BudgetAgg, BudgetWeek, BudgetMonth, BudgetAggDate) {

		$scope.authentication = Authentication;


			$scope.project = Projects.get({
				projectId: $stateParams.projectId
			});

		Budgets.query({project: $stateParams.projectId}, function(response){
			//console.log(response);
		});


		BudgetAgg.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabel = [];
			$scope.budgetValue = []
			angular.forEach(response, function(responseItem){
				$scope.budgetLabel.push(responseItem._id);
				$scope.budgetValue.push(responseItem.total);
			});
			//console.log(response);

			$scope.budgetObj = {};

			for (var i = 0; i < $scope.budgetLabel.length; i++){
				$scope.budgetObj[$scope.budgetLabel[i]] = $scope.budgetValue[i];
			}

		});

		//Query budget aggregate with a date parameter for last week
		console.log(moment().subtract(7, 'days').calendar());

		BudgetWeek.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabelDate7 = [];
			$scope.budgetValueDate7 = [];
			angular.forEach(response, function(responseItem){
				$scope.budgetLabelDate7.push(responseItem._id);
				$scope.budgetValueDate7.push(responseItem.total);
			});
			//console.log(response);
			console.log($scope.budgetLabelDate7);
			console.log($scope.budgetValueDate7);

			$scope.budgetObjDate7= {};

			for (var i = 0; i < $scope.budgetLabelDate7.length; i++){
				$scope.budgetObjDate7[$scope.budgetLabelDate7[i]] = $scope.budgetValueDate7[i];
			}

			console.log($scope.budgetObjDate7);
		});

		//Query budget aggregate with a date parameter for last month
		console.log(moment().subtract(30, 'days').calendar());

		BudgetMonth.query({
			budgetAggId: $stateParams.projectId
		}, function(response){
			$scope.budgetLabelDate30 = [];
			$scope.budgetValueDate30 = [];
			angular.forEach(response, function(responseItem){
				$scope.budgetLabelDate30.push(responseItem._id);
				$scope.budgetValueDate30.push(responseItem.total);
			});
			//console.log(response);
			console.log($scope.budgetLabelDate30);
			console.log($scope.budgetValueDate30);

			$scope.budgetObjDate30 = {};

			for (var i = 0; i < $scope.budgetLabelDate30.length; i++){
				$scope.budgetObjDate30[$scope.budgetLabelDate30[i]] = $scope.budgetValueDate30[i];
			}

			console.log($scope.budgetObjDate30);
		});



		$scope.dt = new Date();

		//$scope.today();

		$scope.clear = function () {
			$scope.dt = null;
		};

		// Disable weekend selection
		$scope.disabled = function(date, mode) {
			return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
		};

		$scope.toggleMin = function() {
			$scope.minDate = $scope.minDate ? null : new Date();
		};
		$scope.toggleMin();

		$scope.open = function($event) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened = true;
		};

		$scope.dateOptions = {
			formatYear: 'yy',
			startingDay: 1
		};

		$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		$scope.format = $scope.formats[0];

		$scope.costToday = function(dt){
			BudgetAggDate.query({
				budgetAggId: $stateParams.projectId,
				date: dt
			}, function(response){
				$scope.budgetLabelToday = [];
				$scope.budgetValueToday = [];
				angular.forEach(response, function(responseItem){
					$scope.budgetLabelToday.push(responseItem._id);
					$scope.budgetValueToday.push(responseItem.total);
				});
				//console.log(response);
				console.log($scope.budgetLabelToday);
				console.log($scope.budgetValueToday);

				$scope.budgetObjToday = {};

				for (var i = 0; i < $scope.budgetLabelToday.length; i++){
					$scope.budgetObjToday[$scope.budgetLabelToday[i]] = $scope.budgetValueToday[i];
				}

				console.log($scope.budgetObjToday);
			});
		};


		Projects.get({
			projectId: $stateParams.projectId
		}, function(projectTotalResponse){

			var inputObj = projectTotalResponse.total;
			var labels = Object.keys(projectTotalResponse.total);
			var totalArray = [];
			var data = [];
			$scope.totals = projectTotalResponse.total;

			for(var key in inputObj){
				data.push(inputObj[key]);
				var tempObj = {};
				tempObj[key] = inputObj[key];
				totalArray.push(tempObj);
			}

			$scope.labels = labels;
			$scope.data = data;



		});

		// do the budget calculations here
		// first get the tickets in this project
		// then check which type of issue it is
		// get the hours and multiply with the cost per hour
		// substract the results from the total
		// save the data into the project item

		//ProjectIssues.query(function(projectIssuesResponse){
		//	angular.forEach(projectIssuesResponse, function(item){
		//		if(item.project._id === $stateParams.projectId){
		//			console.log(item);
		//
		//		}
		//	});
		//});

			Budgets.query(function(budgetsResponse){
				var cost;
				angular.forEach(budgetsResponse, function(budgetsItem){
					if(budgetsItem.projectid._id === $stateParams.projectId){
						//console.log(budgetsItem);

						Projects.get({
							projectId: $stateParams.projectId
						}, function(projectResponse){

							var inputObj = projectResponse.total;
							var labels = Object.keys(projectResponse.total);
							var totalArray = [];
							var data = [];
							$scope.totals = projectResponse.total;

							for(var key in inputObj){
								data.push(inputObj[key]);
								var tempObj = {};
								tempObj[key] = inputObj[key];
								totalArray.push(tempObj);
							}

							var l = [];

							angular.forEach(labels, function(labelItem){

								//console.log(labelItem);
								if(labelItem === budgetsItem.issueType.toLowerCase()){

									if(budgetsItem.issueType.toLowerCase() === 'design'){
										console.log(budgetsItem.issueType.toLowerCase());
										angular.forEach(budgetsItem.issueType.toLowerCase(), function(){
											var total = 0;
											total = total + budgetsItem.cost;
											console.log(total);
										});
									}

								}
							});

						});

						//cost = (function(){
						//	// check where the item belongs to in the
						//	switch(budgetsItem.issueType){
						//		case "Design":
						//				var designCost = 0;
						//				return designCost = designCost + budgetsItem.cost;
						//				console.log(designCost);
						//				console.log("cost");
						//			break;
						//		//case "Analysis":
						//		//	return logResponse.time * item.project.charge.analysis;
						//		//	break;
						//		//case "Coding":
						//		//	return logResponse.time * item.project.charge.coding;
						//		//	break;
						//		//case "Documentation":
						//		//	return logResponse.time * item.project.charge.documentation;
						//		//	break;
						//		//case "Bug":
						//		//	return logResponse.time * item.project.charge.coding;
						//		//	break;
						//	}
                        //
						//});

					}
				});

			});


	}
]);
