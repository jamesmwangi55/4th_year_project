'use strict';

//Setting up route
angular.module('projects').config(['$stateProvider',
	function($stateProvider) {
		// Projects state routing
		$stateProvider.
		state('work-logged', {
			url: '/projects/:projectId/work-logged',
			templateUrl: 'modules/projects/views/work-logged.client.view.html'
		}).
		state('budget', {
			url: '/projects/:projectId/budget',
			templateUrl: 'modules/projects/views/budget.client.view.html'
		}).
		state('project-issue', {
			url: '/projects/:projectId/project-issues',
			templateUrl: 'modules/projects/views/project-issue.client.view.html'
		}).
		state('project-cost', {
			url: '/project-cost/:projectId',
			templateUrl: 'modules/projects/views/project-cost.client.view.html'
		}).
		state('listProjects', {
			url: '/projects',
			templateUrl: 'modules/projects/views/list-projects.client.view.html'
		}).
		state('createProject', {
			url: '/projects/create',
			templateUrl: 'modules/projects/views/create-project.client.view.html'
		}).
		state('viewProject', {
			url: '/projects/:projectId',
			templateUrl: 'modules/projects/views/view-project.client.view.html'
		}).
		state('editProject', {
			url: '/projects/:projectId/edit',
			templateUrl: 'modules/projects/views/edit-project.client.view.html'
		});
	}
]);
