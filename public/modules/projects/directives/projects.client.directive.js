'use strict';

angular.module('projects').directive('projects', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Projects directive logic
				// ...

				element.text('this is the projects directive');
			}
		};
	}
]).directive('myDate', ['$timeout', '$filter', function ($timeout, $filter)
{
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModelController) {
			ngModelController.$parsers.push(function(data) {
				//View -> Model
				return data;
			});
			ngModelController.$formatters.push(function(data) {
				//Model -> View
				return $filter('date')(data, 'yyyy-MM-dd');
			});
		}
	};
}]);
