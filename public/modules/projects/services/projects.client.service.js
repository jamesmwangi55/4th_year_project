'use strict';

//Projects service used to communicate Projects REST endpoints
angular.module('projects').factory('Projects', ['$resource',
	function($resource) {
		return $resource('projects/:projectId', { projectId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('ProjectCost', ['$resource',
	function($resource) {
		return $resource('project-cost/:projectId', { projectId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Budgets', ['$resource',
	function($resource) {
		return $resource('budgets/:budgetId', { budgetId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('BudgetAgg', ['$resource',
	function($resource) {
		return $resource('budgetAgg/:budgetAggId/:date', {budgetAggId: '@_id'}, {date: 'date'},
			{
			
			});
	}
]).factory('ProjectIssueAgg', ['$resource',
	function($resource) {
		return $resource('projectIssueAgg/:projectId/:date', {projectId: '@_id'}, {date: 'date'},
			{

			});
	}
]).factory('AssigneeIssueAgg', ['$resource',
	function($resource) {
		return $resource('assigneeIssueAgg/', {},
			{

			});
	}
]).factory('WorkLogged', ['$resource',
	function($resource) {
		return $resource('logged/:projectId/:date', {projectId: '@_id'}, {date: 'date'},
			{

			});
	}
]).factory('BudgetWeek', ['$resource',
	function($resource) {
		return $resource('budgetWeek/:budgetAggId', { budgetAggId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('BudgetMonth', ['$resource',
		function($resource) {
			return $resource('budgetMonth/:budgetAggId', { budgetAggId: '@_id'
			}, {
				update: {
					method: 'PUT'
				}
			});
		}
]).factory('BudgetAggDate', ['$resource',
	function($resource) {
		return $resource('budgetAgg/:budgetAggId/:date', {budgetAggId: '@_id'}, {date: 'date'},
			{

			});
	}
]).factory('WorkLoggedToday', ['$resource',
	function($resource) {
		return $resource('loggedToday/:projectId', {projectId: '@_id'},
			{

			});
	}
]).factory('WorkLoggedWeek', ['$resource',
	function($resource) {
		return $resource('loggedWeek/:projectId', {projectId: '@_id'},
			{

			});
	}
]).factory('WorkLoggedMonth', ['$resource',
	function($resource) {
		return $resource('loggedMonth/:projectId', {projectId: '@_id'},
			{

			});
	}
]).factory('WorkLoggedDay', ['$resource',
	function($resource) {
		return $resource('loggedDay/:projectId/:date', {projectId: '@_id'}, {date: 'date'},
			{

			});
	}
]);
