'use strict';

angular.module('issues').controller('ViewIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Comments', 'Notify', '$modal', '$log', 'Boards', 'Logs', 'Budgets', '$state',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Comments, Notify, $modal, $log, Boards, Logs, Budgets, $state) {

		$scope.authentication = Authentication;
		$scope.typeArray = ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'];
		$scope.priorityArray = ['Low', 'Medium', 'High'];
		$scope.projects = Projects.query();
		$scope.listUsers = [];
		UsersList.query(function(response){
			angular.forEach(response, function(item){
				if(item.status !== 'inactive'){
					$scope.listUsers.push(item);
				}
			});
		});
		$scope.projectIssues = ProjectIssues.query();
		$scope.items = [];
		$scope.issue = Issues.get({
			issueId: $stateParams.issueId
		}, function(issueResponse){
			$scope.max =  issueResponse.estimate;
		});




		$scope.assigneeIssues = AssigneeIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id === $stateParams.issueId){
					$scope.assigneeIssue = item;
				}
			});
		});

		$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
			angular.forEach(projectIssuesResponse, function(item){
				if(item.issue._id === $stateParams.issueId){
					$scope.projectIssue = item;
				}
			});
		});

		//==============================================================================================
		// Create List that will be displayed when members select the button to assign
		//==============================================================================================

		$scope.userList = [];
		$scope.userList.push($scope.authentication.user);

		UsersList.query(function(assigneeIssuesResponse){
			angular.forEach(assigneeIssuesResponse, function(item){
				if(item.roles === 'admin' || item.roles === 'manager'){

					if(item.status !== 'inactive'){
						$scope.userList.push(item);
					}


				}
			});
			//console.log($scope.userList);
		});



		for(var i = 0; i < $scope.assigneeIssues.length; i++){
			//if($scope.assigneeIssues[i].issue._id == $stateParams.issueId){
			//	$scope.currentIssue = $scope.assigneeIssues[i].issue._id;
			//}
			//console.log($scope.assigneeIssues[i].issue._id );
		}


		// Find existing Issue
		$scope.findOne = function() {
			$scope.issue = Issues.get({
				issueId: $stateParams.issueId
			});
		};

		$scope.addComment = function(){
			var comment = new Comments({
				user: $scope.authentication.user._id,
				issue: $stateParams.issueId,
				comment: $scope.comment
			});

			comment.$save(function(response){
				$state.reload();
				//Notify.sendMsg('NewComment', {'id': response._id});
                //
				//Notify.getMsg('NewComment', function(event, data){
				//	$scope.comments = Comments.query();
				//});

				// clear the comment text area
				$scope.comment = '';

			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Issues
		//$scope.findComments = function() {
		//	$scope.comments = Comments.query();
		//};

		$scope.commentsArray = [];

		$scope.comments = Comments.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id == $stateParams.issueId){
					$scope.commentsArray.push(item);

				}
			});
		});

		$scope.log;

		// log work time
		//$scope.logWork = function(){
        //
		//		//retrieve the log info from database and add the current log to it
		//		var issue = Issues.get({
		//			issueId: $stateParams.issueId
		//		}, function(response){
        //
        //
		//			$scope.issue.log = $scope.issue.log + response.log;
        //
		//			issue.$update(function() {
		//				//$location.path('articles/' + article._id);
		//			}, function(errorResponse) {
		//				$scope.error = errorResponse.data.message;
		//			});
		//		});
        //
        //
		//};

		//// Find existing Assignee Issue
		//$scope.findAssigneeIssue = function() {
		//	$scope.assigneeIssue = AssigneeIssues.get({
		//		assigneeIssueId: $stateParams.assigneeIssueId
		//	});
		//};

		//// Find existing Project Issue
		//$scope.findProjectIssue = function() {
		//	$scope.projectIssue = ProjectIssues.get({
		//		issueId: $stateParams.issueId
		//	});
		//};

		//=================================================================================================
		//Run this code so as to get the column names
		//=================================================================================================
		var projectIssues = ProjectIssues.query(function(projectIssuesResponse){
			angular.forEach(projectIssuesResponse, function(projectIssueItem){
				if(projectIssueItem.issue._id === $stateParams.issueId){
					//$scope.projectIssue = projectIssueItem;
					//console.log($scope.projectIssue.project._id);
					//console.log(projectIssueItem);

					//find all the boards
					var boards = Boards.query(function(boardsResponse){
						angular.forEach(boardsResponse, function(boardsItem){
							//console.log(boardsItem.project._id);
							if(boardsItem.project._id === projectIssueItem.project._id){

								//========================================================================
								//Handle Moving Issue From One Column to Another Here
								//========================================================================

								// Loop through all the  columns
								angular.forEach(boardsItem.column, function(columnItem){
									// loop through all issues in a column
									angular.forEach(columnItem.issues, function(issuesItem){
										if(issuesItem === $stateParams.issueId){
											//console.log(boardsItem.column);

											Array.prototype.last = function () {
												return this[this.length - 1];
											};

											$scope.checkColumns = function(){

												$scope.ifLast = false;

												if(columnItem === boardsItem.column.last()){
													$scope.ifLast = true;
												}

												return $scope.ifLast;
											};
											$scope.columns = boardsItem.column;
											$scope.currentColumn = columnItem.name;
											//console.log(boardsItem.column.indexOf(columnItem));

											//console.log(boardsItem.column.indexOf(columnItem + 1));
											if(columnItem !== boardsItem.column.last()){
												$scope.nextColumn = boardsItem.column[boardsItem.column.indexOf(columnItem) + 1].name;
											}

										}
									});

								});
							}
						});
					});

				}
			});
		});




		//===================================================================================================================
		//Code to log work
		//===================================================================================================================

		$scope.moveIssue = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/move-issue.client.view.html',
				controller: function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (assigneeIssue) {

						var projectIssues = ProjectIssues.query(function(projectIssuesResponse){
							angular.forEach(projectIssuesResponse, function(projectIssueItem){
								if(projectIssueItem.issue._id === $stateParams.issueId){
									//$scope.projectIssue = projectIssueItem;
									//console.log($scope.projectIssue.project._id);
									//console.log(projectIssueItem);

									//find all the boards
									var boards = Boards.query(function(boardsResponse){
										angular.forEach(boardsResponse, function(boardsItem){
											//console.log(boardsItem.project._id);
											if(boardsItem.project._id === projectIssueItem.project._id){

												var board = Boards.get({
													boardId: boardsItem._id
												}, function(boardResponse){

													angular.forEach(boardResponse.column, function(columnItem){
														angular.forEach(columnItem.issues, function(issuesItem){
															if(issuesItem._id === $stateParams.issueId){

																if(columnItem !== boardResponse.column.last()){
																	//delete item from the column
																	columnItem.issues.splice(columnItem.issues.indexOf(issuesItem), 1);

																	//move item to new column
																	boardResponse.column[boardResponse.column.indexOf(columnItem) + 1].issues.push(issuesItem._id);
																}

																//console.log(boardResponse.column.last()._id);
																//console.log(columnItem._id);

																if((boardResponse.column.indexOf(columnItem) + 1) === boardResponse.column.indexOf(boardResponse.column.last())){
																	console.log('issue is in last column');
																	var issue = Issues.get({
																		issueId: $stateParams.issueId
																	}, function(issueResponse){
																		console.log(issueResponse);
																		issueResponse.status  = 'Closed';

																		issue.$update(function(updatedResponse){

																		}, function(errorResponse){
																			$scope.error = errorResponse.data.message;
																		});
																	});
																}

															}
														});
													});

														board.$update(function(updateResponse){
															//console.log(boardResponse.column);

															// reload the page
															//$location.path('boards/' + boardResponse._id).reload();
															//$window.location.reload()
															//$route.reload();
															$state.reload();

														}, function(errorResponse){
															$scope.error = errorResponse.data.message
														});



												});

											}
										});
									});

								}
							});
						});



						//retrieve the log info from database and add the current log to it
						var issue = Issues.get({
							issueId: $stateParams.issueId
						}, function(response){

							issue.$update(function(response) {
								//$location.path('articles/' + article._id);
							}, function(errorResponse) {
								$scope.error = errorResponse.data.message;
							});
						});

						$scope.assignToLead = function(){
							//console.log("hhahhahhaha");
						}


						assigneeIssue.$update(function(updateResponse){

						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});

						//var log = new Logs({
						//	issue: $stateParams.issueId,
						//	user: $scope.authentication.user._id,
						//	time: this.log
						//});

						//log.$save(function(logResponse){
                        //
						//	//console.log(logResponse);
                        //
						//	$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
						//		angular.forEach(projectIssuesResponse, function(item){
						//			if(item.issue._id === $stateParams.issueId){
						//				$scope.projectIssue = item;
						//				//console.log(item);
						//				//console.log($scope.authentication.user._id);
						//				//console.log(item.project._id);
						//				//console.log(item.issue.issueType);
                        //
						//				var cost = (function(){
						//					// check where the item belongs to in the
						//					switch(item.issue.issueType){
						//						case "Design":
						//							return logResponse.time * item.project.charge.design;
						//							break;
						//						case "Analysis":
						//							return logResponse.time * item.project.charge.analysis;
						//							break;
						//						case "Coding":
						//							return logResponse.time * item.project.charge.coding;
						//							break;
						//						case "Documentation":
						//							return logResponse.time * item.project.charge.documentation;
						//							break;
						//						case "Bug":
						//							return logResponse.time * item.project.charge.coding;
						//							break;
						//					}
                        //
						//				});
                        //
                        //
						//				var budget = new Budgets({
						//					user: $scope.authentication.user._id,
						//					projectid: item.project._id,
						//					issueType: item.issue.issueType,
						//					cost: cost()
						//				});
                        //
						//				budget.$save(function(budgetResponse){
                        //
						//					//console.log(budgetResponse);
                        //
						//				}, function(errorResponse){
						//					$scope.error = errorResponse.data.message;
						//				});
						//			}
						//		});
						//	});
                        //
						//	//var budget = new Budgets({
						//	//	user: $scope.authentication.user._id,
						//	//	project:
						//	//	issueType:
						//	//	cost:
						//	//});
                        //
                        //
						//}, function(errorResponse){
						//	$scope.error = errorResponse.data.message;
						//});

						$modalInstance.close();
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				},
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};


		$scope.logTotal = 0;
		var logArray = [];

		Logs.query(function(logsResponse){
			angular.forEach(logsResponse, function(logsResponseItem){
				if(logsResponseItem.issue._id === $stateParams.issueId){
					logArray.push(logsResponseItem);
					$scope.logTotal = $scope.logTotal + logsResponseItem.time;
				}
			});
			//console.log(logArray);
			//for(var i = 0; i < logArray.length; i++){
			//	logTotal = logTotal + logArray[i].time;
			//	console.log(logArray[i].time);
			//}
			//console.log($scope.logTotal);
			$scope.dynamic = $scope.logTotal;

		});

		//change assignee
		$scope.changeAssignee = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/change-assignee.client.view.html',
				controller: function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (assigneeIssue, newComment) {

						var comment = new Comments({
							user: $scope.authentication.user._id,
							issue: $stateParams.issueId,
							comment: newComment
						});

						comment.$save(function(response){
							//$state.reload();
							//Notify.sendMsg('NewComment', {'id': response._id});
							//
							//Notify.getMsg('NewComment', function(event, data){
							//	$scope.comments = Comments.query();
							//});

							// clear the comment text area
							$scope.comment = '';

						}, function(errorResponse){
							//$scope.error = errorResponse.data.message;
						});

						// Find a list of Issues
						//$scope.findComments = function() {
						//	$scope.comments = Comments.query();
						//};

						$scope.commentsArray = [];

						$scope.comments = Comments.query(function(response){
							angular.forEach(response, function(item){
								if(item.issue._id == $stateParams.issueId){
									$scope.commentsArray.push(item);

								}
							});
						});

						assigneeIssue.$update(function(updateResponse){
							$state.reload();
						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});

						$modalInstance.close();

					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				},
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});


			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		//change project
		$scope.changeProject = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/change-project.client.view.html',
				controller: function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (projectIssue, project) {

						//console.log(projectIssue._id);

					ProjectIssues.query(function(projectIssuesResponse){
							angular.forEach(projectIssuesResponse, function(projectIssueItem){
								if(projectIssueItem.issue._id === $stateParams.issueId){
									//$scope.projectIssue = projectIssueItem;
									//console.log($scope.projectIssue.project._id);
									//console.log(projectIssueItem);

									//find all the boards
									var boards = Boards.query(function(boardsResponse){
										angular.forEach(boardsResponse, function(boardsItem){
											//console.log(boardsItem.project._id);
											if(boardsItem.project._id === projectIssueItem.project._id){

												var board = Boards.get({
													boardId: boardsItem._id
												}, function(boardResponse){

													angular.forEach(boardResponse.column, function(columnItem){
														angular.forEach(columnItem.issues, function(issuesItem){
															if(issuesItem._id === $stateParams.issueId){

																if(columnItem !== boardResponse.column.last()){
																	//delete item from the column
																	columnItem.issues.splice(columnItem.issues.indexOf(issuesItem), 1);

																	//move item to new column
																	boardResponse.column[boardResponse.column.indexOf(columnItem) + 1].issues.push(issuesItem._id);
																}

																//console.log(boardResponse.column.last()._id);
																//console.log(columnItem._id);

																if((boardResponse.column.indexOf(columnItem) + 1) === boardResponse.column.indexOf(boardResponse.column.last())){
																	console.log('issue is in last column');
																	var issue = Issues.get({
																		issueId: $stateParams.issueId
																	}, function(issueResponse){
																		console.log(issueResponse);
																		issueResponse.status  = 'Closed';

																		issue.$update(function(updatedResponse){

																		}, function(errorResponse){
																			$scope.error = errorResponse.data.message;
																		});
																	});
																}

															}
														});
													});

													board.$update(function(updateResponse){
														//console.log(boardResponse.column);

														// reload the page
														//$location.path('boards/' + boardResponse._id).reload();
														//$window.location.reload()
														//$route.reload();
														$state.reload();

													}, function(errorResponse){
														$scope.error = errorResponse.data.message
													});



												});

											}
										});
									});

								}
							});
						});

						console.log(project);

						projectIssue.$update(function(updateResponse){

							var boards = Boards.query(function(boardsResponse){
								angular.forEach(boardsResponse, function(boardResponseItem){
									console.log(boardsResponse);
								});
							});
							console.log(updateResponse);
							$state.reload();
							$modalInstance.close();
						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});



					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				},
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});


			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};

		//log work
		$scope.logWork = function (size) {

			var modalInstance = $modal.open({
				templateUrl: 'modules/issues/views/log-work.client.view.html',
				controller: function($scope, $modalInstance, $state, Authentication){

					$scope.authentication = Authentication;

					$scope.ok = function (assigneeIssue, newComment) {

						//var projectIssues = ProjectIssues.query(function(projectIssuesResponse){
						//	angular.forEach(projectIssuesResponse, function(projectIssueItem){
						//		if(projectIssueItem.issue._id === $stateParams.issueId){
						//			//$scope.projectIssue = projectIssueItem;
						//			//console.log($scope.projectIssue.project._id);
						//			//console.log(projectIssueItem);
                        //
						//			//find all the boards
						//			var boards = Boards.query(function(boardsResponse){
						//				angular.forEach(boardsResponse, function(boardsItem){
						//					//console.log(boardsItem.project._id);
						//					if(boardsItem.project._id === projectIssueItem.project._id){
                        //
						//						var board = Boards.get({
						//							boardId: boardsItem._id
						//						}, function(boardResponse){
                        //
						//							angular.forEach(boardResponse.column, function(columnItem){
						//								angular.forEach(columnItem.issues, function(issuesItem){
						//									if(issuesItem._id === $stateParams.issueId){
                        //
						//										if(columnItem !== boardResponse.column.last()){
						//											//delete item from the column
						//											columnItem.issues.splice(columnItem.issues.indexOf(issuesItem), 1);
                        //
						//											//move item to new column
						//											boardResponse.column[boardResponse.column.indexOf(columnItem) + 1].issues.push(issuesItem._id);
						//										}
                        //
						//										//console.log(boardResponse.column.last()._id);
						//										//console.log(columnItem._id);
                        //
						//										if((boardResponse.column.indexOf(columnItem) + 1) === boardResponse.column.indexOf(boardResponse.column.last())){
						//											console.log('issue is in last column');
						//											var issue = Issues.get({
						//												issueId: $stateParams.issueId
						//											}, function(issueResponse){
						//												console.log(issueResponse);
						//												issueResponse.status  = 'Closed';
                        //
						//												issue.$update(function(updatedResponse){
                        //
						//												}, function(errorResponse){
						//													$scope.error = errorResponse.data.message;
						//												});
						//											});
						//										}
                        //
						//									}
						//								});
						//							});
                        //
						//							board.$update(function(updateResponse){
						//								//console.log(boardResponse.column);
                        //
						//								// reload the page
						//								//$location.path('boards/' + boardResponse._id).reload();
						//								//$window.location.reload()
						//								//$route.reload();
						//								$state.reload();
                        //
						//							}, function(errorResponse){
						//								$scope.error = errorResponse.data.message
						//							});
                        //
                        //
                        //
						//						});
                        //
						//					}
						//				});
						//			});
                        //
						//		}
						//	});
						//});

							var comment = new Comments({
								user: $scope.authentication.user._id,
								issue: $stateParams.issueId,
								comment: newComment
							});

							comment.$save(function(response){
								//$state.reload();
								//Notify.sendMsg('NewComment', {'id': response._id});
								//
								//Notify.getMsg('NewComment', function(event, data){
								//	$scope.comments = Comments.query();
								//});

								// clear the comment text area
								$scope.comment = '';

							}, function(errorResponse){
								//$scope.error = errorResponse.data.message;
							});

						// Find a list of Issues
						//$scope.findComments = function() {
						//	$scope.comments = Comments.query();
						//};

						$scope.commentsArray = [];

						$scope.comments = Comments.query(function(response){
							angular.forEach(response, function(item){
								if(item.issue._id == $stateParams.issueId){
									$scope.commentsArray.push(item);

								}
							});
						});



						//retrieve the log info from database and add the current log to it
						var issue = Issues.get({
							issueId: $stateParams.issueId
						}, function(response){

							issue.$update(function(response) {
								//$location.path('articles/' + article._id);
							}, function(errorResponse) {
								$scope.error = errorResponse.data.message;
							});
						});

						$scope.assignToLead = function(){
							//console.log("hhahhahhaha");
						}


						assigneeIssue.$update(function(updateResponse){

						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});

						var log = new Logs({
							issue: $stateParams.issueId,
							user: $scope.authentication.user._id,
							time: this.log
						});

						log.$save(function(logResponse){

							//console.log(logResponse);

							$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
								angular.forEach(projectIssuesResponse, function(item){
									if(item.issue._id === $stateParams.issueId){
										$scope.projectIssue = item;
										//console.log(item);
										//console.log($scope.authentication.user._id);
										//console.log(item.project._id);
										//console.log(item.issue.issueType);

										var cost = (function(){
											// check where the item belongs to in the
											switch(item.issue.issueType){
												case "Design":
													return logResponse.time * item.project.charge.design;
													break;
												case "Analysis":
													return logResponse.time * item.project.charge.analysis;
													break;
												case "Coding":
													return logResponse.time * item.project.charge.coding;
													break;
												case "Documentation":
													return logResponse.time * item.project.charge.documentation;
													break;
												case "Bug":
													return logResponse.time * item.project.charge.coding;
													break;
											}

										});


										var budget = new Budgets({
											user: $scope.authentication.user._id,
											projectid: item.project._id,
											issueType: item.issue.issueType,
											cost: cost(),
											hours: logResponse.time
										});

										budget.$save(function(budgetResponse){

											//console.log(budgetResponse);

										}, function(errorResponse){
											$scope.error = errorResponse.data.message;
										});
									}
								});
							});

							//var budget = new Budgets({
							//	user: $scope.authentication.user._id,
							//	project:
							//	issueType:
							//	cost:
							//});

							if(logResponse){
								$modalInstance.close();
								$state.reload();
							}


						}, function(errorResponse){
							$scope.error = errorResponse.data.message;
						});


					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};

				},
				size: size,
				resolve: {
					items: function () {
						return $scope.items;
					}
				}
			});

			modalInstance.result.then(function (selectedItem) {
				$scope.selected = selectedItem;
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};
	}
]);
