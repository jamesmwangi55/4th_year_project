'use strict';

angular.module('issues').controller('ListIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues',
	function($scope, $stateParams, $location, Authentication, Issues) {
		// Find a list of Issues
		$scope.find = function() {
			$scope.issues = Issues.query();
		};
	}
]);
