'use strict';

angular.module('issues').controller('EditIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues', 'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues) {


		$scope.authentication = Authentication;
		$scope.typeArray = ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'];
		$scope.priorityArray = ['Low', 'Medium', 'High'];
		$scope.projects = Projects.query();
		$scope.listUsers = UsersList.query();


		//==============================================================================================
		// Create List that will be displayed when members select the button to assign
		//==============================================================================================
		$scope.userList = [];

		AssigneeIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id === $stateParams.issueId){
					//console.log(item.assignee);

					$scope.userList.push($scope.authentication.user);

					ProjectIssues.query(function(projectIssuesResponse){
						angular.forEach(projectIssuesResponse, function(item){
							if(item.issue._id === $stateParams.issueId){
								Projects.get({
									projectId: item.project._id
								}, function(projectResponse){
									//console.log(projectResponse.lead);
									$scope.userList.push(projectResponse.lead);
								});
							}
						});
					});

				}
			});
			//console.log($scope.userList);
		});

		var assigneeIssue = {};

		$scope.assigneeIssues = AssigneeIssues.query(function(response){
			angular.forEach(response, function(item){
				if(item.issue._id === $stateParams.issueId){
					//console.log(item);
					$scope.assigneeIssue = item;
					//console.log($scope.assigneeIssue);
					assigneeIssue = AssigneeIssues.get({
						assigneeIssueId: item._id
					});
				}
			});
		});

		var projectIssue = {};

		$scope.projectIssues = ProjectIssues.query(function(projectIssuesResponse){
			angular.forEach(projectIssuesResponse, function(item){
				if(item.issue._id === $stateParams.issueId){
					console.log(item);
					$scope.projectIssue = item;
					projectIssue = ProjectIssues.get({
						projectIssueId: item._id
					});
					//console.log(projectIssue);
				}
			});
		});

		//console.log(projectIssue);
		//console.log(assigneeIssue);


		// Update existing Issue
		$scope.update = function() {
			var issue = $scope.issue;

			assigneeIssue.$update(function(){

			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			});

			projectIssue.$update(function(){

			}, function(errorResponse){
				$scope.error = errorResponse.data.message;
			});

			issue.$update(function() {
				$location.path('issues/' + issue._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Find existing Issue
		$scope.findOne = function() {
			$scope.issue = Issues.get({
				issueId: $stateParams.issueId
			});
		};


		// Remove existing Issue
		$scope.remove = function(issue) {
			if ( issue ) {
				issue.$remove();

				for (var i in $scope.issues) {
					if ($scope.issues [i] === issue) {
						$scope.issues.splice(i, 1);
					}
				}
			} else {
				$scope.issue.$remove(function() {
					$location.path('issues');
				});
			}
		};

	}

]);
