'use strict';

angular.module('issues').controller('CreateIssueController', ['$scope', '$stateParams', '$location', 'Authentication', 'Issues',  'Projects', 'UsersList', 'AssigneeIssues', 'ProjectIssues', 'Boards',
	function($scope, $stateParams, $location, Authentication, Issues, Projects, UsersList, AssigneeIssues, ProjectIssues, Boards) {

		$scope.authentication = Authentication;
		$scope.typeArray = ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'];
		$scope.priorityArray = ['Low', 'Medium', 'High'];
		$scope.projects = [];
		Projects.query(function(projectResponse){
			angular.forEach(projectResponse, function(item){
				if(item.status !== 'Closed'){
					$scope.projects.push(item);
				}
			});
		});
		$scope.listUsers = [];
		UsersList.query(function(response){
			angular.forEach(response, function(item){
				if(item.status !== 'inactive'){
					$scope.listUsers.push(item);
				}
			});
		});

		//==============================================================================================
		// Create List that will be displayed when members select the button to assign
		//==============================================================================================

					$scope.userList = [];
					$scope.userList.push($scope.authentication.user);

						UsersList.query(function(assigneeIssuesResponse){
							angular.forEach(assigneeIssuesResponse, function(item){
								if(item.roles === 'admin' || item.roles === 'manager'){

									if(item.status !== 'inactive'){
										$scope.userList.push(item);
									}


								}
							});
							//console.log($scope.userList);
						});


		$scope.selectProject = function(){
			return $scope.selectedProject;
		};

		// Create new Issue
		$scope.create = function() {

			if($scope.selectedProject == undefined){
				$scope.error = 'Please Choose Project';
			}


			// Create new Issue object
			var issue = new Issues ({
				name: $scope.selectedProject.name,
				created: this.created,
				reporter: $scope.authentication.user._id,
				issueType: this.issueType,
				summary: this.summary,
				priority: this.priority,
				description: this.description,
				attachment: this.attachment,
				estimate: this.estimate,
				company: $scope.authentication.user.company
			});

			// Redirect after save
			issue.$save(function(response) {


				if($scope.selectedAssignee == undefined){
					$scope.error = 'Please Choose Assignee';
					return;
				}

				if($scope.selectedProject == undefined){
					$scope.error = 'Please Choose Project';
					return;
				}

				// create new assigneeIssue item
				var assigneeIssue = new AssigneeIssues({
					issue: response._id,
					assignee: $scope.selectedAssignee
				});

				assigneeIssue.$save(function(response){

				}, function(errorResponse){
					$scope.error = errorResponse.data.message;
				});

				// create new projectIssue item
				var projectIssue = new ProjectIssues({
					issue: response._id,
					project: $scope.selectedProject._id
				});

				projectIssue.$save(function(response){

				}, function(errorResponse){
					$scope.error = errorResponse.data.message;
				});

				//add issue created to to To Do column in board

				// get all board and loop checking for the board belonging to the current project
				var getBoards = Boards.query(function(boardsResponse){
					angular.forEach(boardsResponse, function(item){
						if(item.project._id === projectIssue.project){



							var board = Boards.get({
								boardId: item._id
							}, function(boardResponse){
								boardResponse.column[0].issues.push(response._id)
								//console.log(boardResponse.column[0].issues);

								board.$update(function(updateResponse){
									console.log(boardResponse.column[0].issues);
								}, function(errorResponse){
									$scope.error = errorResponse.data.message
								});
							});

						}
					});
				});



				$location.path('issues/' + response._id);
				$scope.name = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};
	}
]);
