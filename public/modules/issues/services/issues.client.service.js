'use strict';

//Issues service used to communicate Issues REST endpoints
angular.module('issues').factory('Issues', ['$resource',
	function($resource) {
		return $resource('issues/:issueId', { issueId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Upload', ['$resource',
	function($resource) {
		return $resource('/upload/:filename', { filename: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('AssigneeIssues', ['$resource',
	function($resource) {
		return $resource('assigneeIssues/:assigneeIssueId', { assigneeIssueId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			query:  {
				method:'GET', isArray:true
			}
		});
	}
]).factory('ProjectIssues', ['$resource',
	function($resource) {
		return $resource('projectIssues/:projectIssueId', { projectIssueId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			query:  {
				method:'GET', isArray:true
			}
		});
	}
]).factory('Comments', ['$resource',
	function($resource) {
		return $resource('comments/:commentId', { commentId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Notify', ['$rootScope', function($rootScope) {
		var notify = [];

		notify.sendMsg = function(msg, data){
			data = data || {};
			$rootScope.$emit(msg, data);

			console.log('message sent');
		}

		notify.getMsg = function(msg, func, scope){
			var unbind = $rootScope.$on(msg, func);

			if(scope){
				scope.$on('destroy', unbind);
			}
		}

		return notify;
	}
]).factory('Logs', ['$resource',
	function($resource) {
		return $resource('logs/:logId', { logId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
