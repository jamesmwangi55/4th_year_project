'use strict';

angular.module('issues').directive('issues', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Issues directive logic
				// ...

				element.text('this is the issues directive');
			}
		};
	}
]).directive('ngEnter', function(){
	return function(scope, element, attrs){
		element.bind('keydown keypress', function(event){
			if(event.which === 13){
				scope.$apply(function(){
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
}).directive('renderOnBlur', function() {
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, elm, attrs, ctrl) {
			elm.bind('blur', function() {
				var viewValue = ctrl.$modelValue;
				for (var i in ctrl.$formatters) {
					viewValue = ctrl.$formatters[i](viewValue);
				}
				ctrl.$viewValue = viewValue;
				ctrl.$render();
			});
		}
	};
});;
