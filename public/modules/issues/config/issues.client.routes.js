'use strict';

//Setting up route
angular.module('issues').config(['$stateProvider',
	function($stateProvider) {
		// Issues state routing
		$stateProvider.
		state('change-project', {
			url: '/change-project',
			templateUrl: 'modules/issues/views/change-project.client.view.html'
		}).
		state('change-assignee', {
			url: '/change-assignee',
			templateUrl: 'modules/issues/views/change-assignee.client.view.html'
		}).
		state('log-work', {
			url: '/log-work',
			templateUrl: 'modules/issues/views/log-work.client.view.html'
		}).
		state('move-issue', {
			url: '/move-issue/:boardId',
			templateUrl: 'modules/issues/views/move-issue.client.view.html'
		}).
		state('listIssues', {
			url: '/issues',
			templateUrl: 'modules/issues/views/list-issues.client.view.html'
		}).
		state('createIssue', {
			url: '/issues/create',
			templateUrl: 'modules/issues/views/create-issue.client.view.html'
		}).
		state('viewIssue', {
			url: '/issues/:issueId',
			templateUrl: 'modules/issues/views/view-issue.client.view.html'
		}).
		state('editIssue', {
			url: '/issues/:issueId/edit',
			templateUrl: 'modules/issues/views/edit-issue.client.view.html'
		});
	}
]);
