'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	AssigneeIssue = mongoose.model('AssigneeIssue');

/**
 * Globals
 */
var user, assigneeIssue;

/**
 * Unit tests
 */
describe('Assignee issue Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			assigneeIssue = new AssigneeIssue({
				// Add model fields
				// ...
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return assigneeIssue.save(function(err) {
				should.not.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		AssigneeIssue.remove().exec();
		User.remove().exec();

		done();
	});
});