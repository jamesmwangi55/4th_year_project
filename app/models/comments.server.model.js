'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Comments Schema
 */
var CommentsSchema = new Schema({
	comment:{
		type: String,
		required: 'Please add a comment'
	},
	issue: {
		type: Schema.ObjectId,
		ref: 'Issue'
	},
	created:{
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Comment', CommentsSchema);
