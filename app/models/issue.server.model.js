'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	filePluginLib = require('mongoose-file'),
	filePlugin = filePluginLib.filePlugin,
	make_upload_to_model = filePluginLib.make_upload_to_model,
	path = require('path'),
	shortId = require('shortid'),
	Schema = mongoose.Schema;

//var uploads_base = path.join(__dirname, "uploads");
//var uploads = path.join(uploads_base, "u");

var AttachmentSchema = new Schema({
	attachment: {
		type: Buffer
	}
});


/**
 * Issue Schema
 */
var IssueSchema = new Schema({
	name: {
		type: String,
		default: '',
		unique: true,
		trim: true,
		required: "Please Choose Project"
	},
	created: {
		type: Date,
		default: Date.now
	},
	reporter: {
		type: Schema.ObjectId,
		ref: 'User',
		required: "Please Choose Reporter"
	},
	issueType: {
		type: String,
		enum: ['Analysis', 'Design', 'Documentation', 'Testing', 'Coding', 'Bug'],
		required: 'Please Choose Issue Type'
	},
	summary: {
		type: String,
		required: "Please Fill Summary"
	},
	priority: {
		type: String,
		enum: ['Low', 'Medium', 'High'],
		required: 'Please Select Issue Priority'
	},
	description: {
		type: String,
		required: "Please Fill Description"
	},
	attachment: [
		AttachmentSchema
	],
	estimate: {
		type: Number,
		required: 'Please Enter The Time'
	},
	log:{
		type: Number,
		default: 0
	},
	newEstimate: {
		type: Number,
		default: 0
	},
	company: {
		type: String
	},
	status: {
		type: String,
		enum: ['Open', 'Closed'],
		default: 'Open'
	}
});

//IssueSchema.plugin(filePlugin, {
//	name: "attachment",
//	upload_to: make_upload_to_model(uploads, 'attachment'),
//	relative_to: uploads_base
//});

mongoose.model('Issue', IssueSchema);
