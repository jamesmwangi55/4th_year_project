'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * ProjectIssue Schema
 */
var ProjectIssueSchema = new Schema({
	issue: {
		type: Schema.ObjectId,
		ref: 'Issue'
	},
	project : {
		type: Schema.ObjectId,
		ref: 'Project',
		required: 'Please Select Project'
	}
});

mongoose.model('ProjectIssue', ProjectIssueSchema);
