'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Logs Schema
 */
var LogsSchema = new Schema({
	issue: {
		type: Schema.ObjectId,
		ref: 'Issue'
	},
	user:{
		type: Schema.ObjectId,
		ref: 'User'
	},
	time: {
		type: Number,
		required: 'Please Fill In The Time Worked'
	},
	logged: {
		type: Date,
		default: Date.now
	}

});

mongoose.model('Log', LogsSchema);
