'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * AssigneeIssue Schema
 */
var AssigneeIssueSchema = new Schema({
	issue: {
		type: Schema.ObjectId,
		ref: 'Issue'
	},
	assignee: {
		type: Schema.ObjectId,
		ref: 'User',
		required: "Please Select Assignee"
	}
});

mongoose.model('AssigneeIssue', AssigneeIssueSchema);
