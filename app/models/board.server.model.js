'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Board Schema
 */


var ColumnSchema = new Schema({
	name:{
		type: String,
		default: '',
		required: 'Please fill Column name',
		trim: true
	},
	issues: [
		{
			type: Schema.ObjectId,
			ref: 'Issue'
		}
	]
});

var BoardSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Board name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	project:{
		type: Schema.ObjectId,
		ref: 'Project'
	},
	column: [
		ColumnSchema
	],
	company:{
		type: String
	}
});

mongoose.model('Board', BoardSchema);
