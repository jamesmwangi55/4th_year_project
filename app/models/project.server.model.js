'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Project Schema
 */



var ProjectSchema = new Schema({
	name: {
		type: String,
		default: '',
		trim: true,
		unique: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	lead: {
		type: Schema.ObjectId,
		ref: 'User',
		required: 'Please choose Project Lead'
	},
	dueDate:{
		type: Date,
		required: 'Please fill Project Due Date'
	},
	hours:{
		analysis: {type: Number, default: 0},
		design: {type: Number, default: 0},
		coding: {type: Number, default: 0},
		testing: {type: Number, default: 0},
		documentation: {type: Number, default: 0}
	},
	charge:{
		analysis: {type: Number, default: 0},
		design: {type: Number, default: 0},
		coding: {type: Number, default: 0},
		testing: {type: Number, default: 0},
		documentation: {type: Number, default: 0}
	},
	total:{
		analysis: {type: Number, default: 0},
		design: {type: Number, default: 0},
		coding: {type: Number, default: 0},
		testing: {type: Number, default: 0},
		documentation: {type: Number, default: 0}
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	company: {
		type: String
	},
	status: {
		type: String,
		enum: ['Open', 'Closed'],
		default: 'Open'
	}
});


mongoose.model('Project', ProjectSchema);
