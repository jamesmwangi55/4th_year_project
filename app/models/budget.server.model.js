'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Budget Schema
 */
var BudgetSchema = new Schema({
	projectid: {
		type: Schema.ObjectId,
		ref: 'Project'
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	created: {
		type: Date,
		default: Date.now
	},
	issueType: {
		type: String,
		default: ''
	},
	cost: {
		type: Number,
		default: 0
	},
	hours: {
		type: Number,
		default: 0
	}
});

mongoose.model('Budget', BudgetSchema);
