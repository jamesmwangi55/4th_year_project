'use strict';

module.exports = function(app) {

    var users = require('../../app/controllers/users.server.controller');
    var assigneeIssues = require('../../app/controllers/assignee-issue.server.controller');

    // Issues Routes
    app.route('/assigneeIssues')
        .get(assigneeIssues.list)
        .post(users.requiresLogin, assigneeIssues.create);

    app.route('/assigneeIssues/:assigneeIssueId')
        .get(assigneeIssues.read)
        .put(users.requiresLogin, assigneeIssues.update)
        .delete(users.requiresLogin,  assigneeIssues.delete);

};
