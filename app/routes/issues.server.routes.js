'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var issues = require('../../app/controllers/issues.server.controller');

	var assigneeIssues = require('../../app/controllers/assignee-issue.server.controller');
	var projectIssues = require('../../app/controllers/project-issue.server.controller');
	var comments = require('../../app/controllers/comments.server.controller');
	var logs = require('../../app/controllers/logs.server.controller');

	//Assignee Issues Routes
	app.route('/assigneeIssues')
		.get(assigneeIssues.list)
		.post(users.requiresLogin, assigneeIssues.create);


	app.route('/assigneeIssues/:assigneeIssueId')
		.get(assigneeIssues.read)
		.put(users.requiresLogin, assigneeIssues.update)
		.delete(users.requiresLogin,  assigneeIssues.delete);

	//Project Issues Routes
	app.route('/projectIssues')
		.get(projectIssues.list)
		.post(users.requiresLogin, projectIssues.create);


	app.route('/projectIssues/:projectIssueId')
		.get(projectIssues.read)
		.put(users.requiresLogin, projectIssues.update)
		.delete(users.requiresLogin,  projectIssues.delete);

	//Comments Routes
	app.route('/comments')
		.get(comments.list)
		.post(users.requiresLogin, comments.create);


	app.route('/comments/:commentId')
		.get(comments.read)
		.put(users.requiresLogin, comments.update)
		.delete(users.requiresLogin,  comments.delete);

	// Logs Routes
	app.route('/logs')
		.get(logs.list)
		.post(users.requiresLogin, logs.create);

	app.route('/logs/:logId')
		.get(logs.read)
		.put(users.requiresLogin, logs.update)
		.delete(users.requiresLogin, logs.delete);

	// Issues Routes
	app.route('/issues')
		.get(issues.list)
		.post(users.requiresLogin, issues.create);

	app.route('/issues/:issueId')
		.get(issues.read)
		.put(users.requiresLogin, issues.update)
		.delete(users.requiresLogin, issues.delete);

	app.route('/upload/:filename').
		get(issues.readUpload);
	app.route('/upload').
		post(issues.createUpload);

	// Finish by binding the Issue middleware
	app.param('issueId', issues.issueByID);
	app.param('assigneeIssueId', assigneeIssues.assigneeIssueByID);
	app.param('projectIssueId', projectIssues.projectIssueByID);
	app.param('logId', logs.logByID);
};
