'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var projects = require('../../app/controllers/projects.server.controller');
	var budget = require('../../app/controllers/budget.server.controller');
	var projectIssues = require('../../app/controllers/project-issue.server.controller');
	var assigneeIssues = require('../../app/controllers/assignee-issue.server.controller');
	var logs = require('../../app/controllers/logs.server.controller');

	// Projects Routes
	app.route('/projects')
		.get(projects.list)
		.post(users.requiresLogin, projects.create);

	app.route('/projects/:projectId')
		.get(projects.read)
		.put(users.requiresLogin, projects.hasAuthorization, projects.update)
		.delete(users.requiresLogin, projects.hasAuthorization, projects.delete);

	// Budget Routes
	app.route('/budgets')
		.get(budget.list, budget.budgetAggregate)
		.post(users.requiresLogin, budget.create);

	app.route('/budgets/:budgetId')
		.get(budget.read)
		.put(users.requiresLogin, budget.update)
		.delete(users.requiresLogin, budget.delete);


	// access budget aggregation framework
	app.route('/budgetAgg/:budgetAggId')
		.get(budget.budgetAggregate);
		//.post(users.requiresLogin, budget.create);

	app.route('/budgetAgg/:budgetAggId/:date')
		.get(budget.budgetAggregateDate);
	//.post(users.requiresLogin, budget.create);

	app.route('/budgetWeek/:budgetAggId')
		.get(budget.budgetAggregateWeek);
	//.post(users.requiresLogin, budget.create);

	app.route('/budgetMonth/:budgetAggId')
		.get(budget.budgetAggregateMonth);
	//.post(users.requiresLogin, budget.create);

	// access work logged aggregation framework
	app.route('/logged/:projectId')
		.get(budget.workLogged);

	app.route('/loggedToday/:projectId')
		.get(budget.workLoggedToday);

	app.route('/loggedWeek/:projectId')
		.get(budget.workLoggedWeek);

	app.route('/loggedMonth/:projectId')
		.get(budget.workLoggedMonth);

	app.route('/loggedDay/:projectId/:date')
		.get(budget.workLoggedDay);


	// access the project-issue aggregation framework
	app.route('/projectIssueAgg/:projectId')
		.get(projectIssues.projectIssueAggregate);
	//.post(users.requiresLogin, budget.create);

	//app.route('/projectIssueAgg/:projectId/:date')
	//	.get(budget.budgetAggregateDate);
	//.post(users.requiresLogin, budget.create);

	// access the project-issue aggregation framework
	app.route('/assigneeIssueAgg')
		.get(assigneeIssues.assigneeIssueAggregate);


	app.route('/project-cost/:projectId')
		.get(projects.read)
		.put(users.requiresLogin, projects.hasAuthorization, projects.update);

	// access work aggregation framework
	//app.route('/logged/:projectId')
	//	.get(logs.workLogged);
	//.post(users.requiresLogin, budget.create);

	//app.route('/logged/:budgetAggId/:date')
	//	.get(budget.budgetAggregateDate);
	////.post(users.requiresLogin, budget.create);

	// Finish by binding the Project middleware
	app.param('projectId', projects.projectByID);
	//app.param('budgetAggId', budget.budgetAggregate);
	//app.param('budgetId', budget.budgetByID);
};
