'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    AssigneeIssue = mongoose.model('AssigneeIssue'),
    _ = require('lodash');

/**
 * Create a Assignee issue
 */
exports.create = function(req, res) {
    var assigneeIssue = new AssigneeIssue(req.body);
    assigneeIssue.user = req.user;

    assigneeIssue.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(assigneeIssue);
        }
    });
};

/**
 * Show the current Assignee issue
 */
exports.read = function(req, res) {
    res.jsonp(req.assigneeIssue);
};

/**
 * Update a Assignee issue
 */
exports.update = function(req, res) {
    var assigneeIssue = req.assigneeIssue ;

    assigneeIssue = _.extend(assigneeIssue , req.body);

    assigneeIssue.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(assigneeIssue);
        }
    });
};

/**
 * Delete an Assignee issue
 */
exports.delete = function(req, res) {

};

/**
 * List of Assignee issues
 */
exports.list = function(req, res) {
    AssigneeIssue.find().populate('assignee').populate('issue').exec(function(err, assigneeIssues) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(assigneeIssues);



        }
    });
};

/**
 * AssigneeIssue middleware
 */
exports.assigneeIssueByID = function(req, res, next, id) {
    AssigneeIssue.findById(id).populate('assignee').populate('issue').exec(function(err, assigneeIssue) {
        if (err) return next(err);
        if (! assigneeIssue) return next(new Error('Failed to load AssigneeIssue ' + id));
        req.assigneeIssue = assigneeIssue ;
        next();
    });
};


// aggregate the results
exports.assigneeIssueAggregate = function(req, res){
    console.log(req.params);
    AssigneeIssue.aggregate()
        .group({_id: '$assignee'})
        .sort('_id')
        .exec(function(err, assigneeIssues) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(assigneeIssues);
            }
        });
};
