'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Issue = mongoose.model('Issue'),
	shortId = require('shortid'),
	_ = require('lodash');

var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
var gfs = new Grid(mongoose.connection.db);

/**
 * Create a Issue
 */
exports.create = function(req, res) {
	var issue = new Issue(req.body);
	issue.user = req.user;

	issue.name =  issue.name + ' - ' + shortId.generate();

	issue.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(issue);
		}
	});
};

/**
 * Show the current Issue
 */
exports.read = function(req, res) {
	res.jsonp(req.issue);
};

/**
 * Update a Issue
 */
exports.update = function(req, res) {
	var issue = req.issue ;

	issue = _.extend(issue , req.body);

	issue.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(issue);
		}
	});
};

/**
 * Delete an Issue
 */
exports.delete = function(req, res) {
	var issue = req.issue ;

	issue.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(issue);
		}
	});
};

/**
 * List of Issues
 */
exports.list = function(req, res) { 
	Issue.find({company: req.user.company}).sort('-created').populate('reporter', 'displayName').exec(function(err, issues) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(issues);
		}
	});
};


exports.createUpload = function(req, res) {


	var part = req.files.filefield;

	var writeStream = gfs.createWriteStream({
		filename: part.name,
		mode: 'w',
		content_type:part.mimetype
	});


	writeStream.on('close', function() {
		return res.status(200).send({
			message: 'Success'
		});
	});

	writeStream.write(part.data);

	writeStream.end();

};


exports.readUpload = function(req, res) {

	gfs.files.find({ filename: req.params.filename }).toArray(function (err, files) {

		if(files.length===0){
			return res.status(400).send({
				message: 'File not found'
			});
		}

		res.writeHead(200, {'Content-Type': files[0].contentType});

		var readstream = gfs.createReadStream({
			filename: files[0].filename
		});

		readstream.on('data', function(data) {
			res.write(data);
		});

		readstream.on('end', function() {
			res.end();
		});

		readstream.on('error', function (err) {
			console.log('An error occurred!', err);
			throw err;
		});
	});

};

/**
 * Issue middleware
 */
exports.issueByID = function(req, res, next, id) {
	Issue.findById(id).populate('reporter', 'displayName').exec(function(err, issue) {
		if (err) return next(err);
		if (! issue) return next(new Error('Failed to load Issue ' + id));
		req.issue = issue ;
		next();
	});
};

/**
 * Issue authorization middleware
 */
//exports.hasAuthorization = function(req, res, next) {
//	if (req.issue.reporter.id !== req.user.id) {
//		return res.status(403).send('User is not authorized');
//	}
//	next();
//};
