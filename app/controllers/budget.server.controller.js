'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Budget = mongoose.model('Budget'),
    moment = require('moment'),
    _ = require('lodash');

/**
 * Create a Budget
 */
exports.create = function(req, res) {
    var budget = new Budget(req.body);
    budget.user = req.user;

    budget.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(budget);
        }
    });
};

/**
 * Show the current Budget
 */
exports.read = function(req, res) {

};

/**
 * Update a Budget
 */
exports.update = function(req, res) {

};

/**
 * Delete an Budget
 */
exports.delete = function(req, res) {

};

/**
 * List of Budgets
 */
exports.list = function(req, res) {
    Budget.find().sort('-created').populate('user', 'displayName').populate('projectid').exec(function(err, budgets) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(budgets);
        }
    });
};

// aggregate the results
exports.budgetAggregate = function(req, res){
    console.log(req.params);
    Budget.aggregate().match({projectid: new mongoose.Types.ObjectId(req.params.budgetAggId)}).group({_id: '$issueType', total:{$sum: '$cost'}, hours:{$sum: '$hours'}}).sort('_id').exec(function(err, budgets) {
        if (err) {
           console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
        } else {
            res.jsonp(budgets);
        }
    });
};

// aggregate the results with dates
exports.budgetAggregateWeek = function(req, res){
    //console.log(req.params);
    console.log(Date(req.params.date));
    console.log(new Date(moment().toDate()));
    console.log(new Date(moment().subtract(1, 'days').toDate()));
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.budgetAggId)})
        .match({created: {$gte: new Date(moment().subtract(7, 'days').toDate()), $lt: new Date(moment().toDate())}})
        .group({_id: '$issueType', total:{$sum: '$cost'}, hours:{$sum: '$hours'}})
        .sort('_id')
        .exec(function(err, budgets) {
        if (err) {
            console.log(err);
            res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(budgets);
        }
    });
};

// aggregate the results with dates
exports.budgetAggregateMonth = function(req, res){
    //console.log(req.params);
    console.log(Date(req.params.date));
    console.log(new Date(moment().toDate()));
    console.log(new Date(moment().subtract(30, 'days').toDate()));
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.budgetAggId)})
        .match({created: {$gte: new Date(moment().subtract(7, 'days').toDate()), $lt: new Date(moment().toDate())}})
        .group({_id: '$issueType', total:{$sum: '$cost'}, hours:{$sum: '$hours'}})
        .sort('_id')
        .exec(function(err, budgets) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(budgets);
            }
        });
};

// aggregate the results with dates
exports.budgetAggregateDate = function(req, res){
    //console.log(req.params);
    console.log(Date(req.params.date));
    console.log(new Date(moment().toDate()));
    console.log(new Date(moment().subtract(1, 'days').toDate()));
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.budgetAggId)})
        .match({created: {$gte: new Date(moment().subtract(1, 'days').toDate()), $lt: new Date(req.params.date)}})
        .group({_id: '$issueType', total:{$sum: '$cost'}, hours:{$sum: '$hours'}})
        .sort('_id')
        .exec(function(err, budgets) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(budgets);
            }
        });
};

//work logged by developer

exports.workLogged = function(req, res){
    //console.log(req.params);
    //console.log(Date(req.params.date));
    console.log(req.params.projectId);
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.projectId)},
        {created: Date(req.params.date)})
        .group({_id: '$user', hours:{$sum: '$hours'}})
        .project({user: '$_id', hours: '$hours'})
        .sort('_id')
        .exec(function(err, logs) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                //res.jsonp(logs);
                Budget.populate(logs, {path: 'user'}, function(err, response){
                    if (err) {
                        console.log(err);
                        res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }else{
                        res.jsonp(response);
                    }
                });

            }
        });
};

//work logged by developer

exports.workLoggedToday = function(req, res){
    //console.log(req.params);
    //console.log(Date(req.params.date));
    console.log(req.params.projectId);
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.projectId)})
        .match({created: {$gte: new Date(moment().subtract(1, 'days').toDate()), $lt: new Date(moment().toDate())}})
        .group({_id: '$user', hours:{$sum: '$hours'}})
        .project({user: '$_id', hours: '$hours'})
        .sort('_id')
        .exec(function(err, logs) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                //res.jsonp(logs);
                Budget.populate(logs, {path: 'user'}, function(err, response){
                    if (err) {
                        console.log(err);
                        res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }else{
                        res.jsonp(response);
                    }
                });

            }
        });
};

exports.workLoggedWeek = function(req, res){
    //console.log(req.params);
    //console.log(Date(req.params.date));
    console.log(req.params.projectId);
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.projectId)})
        .match({created: {$gte: new Date(moment().subtract(7, 'days').toDate()), $lt: new Date(moment().toDate())}})
        .group({_id: '$user', hours:{$sum: '$hours'}})
        .project({user: '$_id', hours: '$hours'})
        .sort('_id')
        .exec(function(err, logs) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                //res.jsonp(logs);
                Budget.populate(logs, {path: 'user'}, function(err, response){
                    if (err) {
                        console.log(err);
                        res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }else{
                        res.jsonp(response);
                    }
                });

            }
        });
};

exports.workLoggedMonth = function(req, res){
    //console.log(req.params);
    //console.log(Date(req.params.date));
    console.log(req.params.projectId);
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.projectId)})
        .match({created: {$gte: new Date(moment().subtract(30, 'days').toDate()), $lt: new Date(moment().toDate())}})
        .group({_id: '$user', hours:{$sum: '$hours'}})
        .project({user: '$_id', hours: '$hours'})
        .sort('_id')
        .exec(function(err, logs) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                //res.jsonp(logs);
                Budget.populate(logs, {path: 'user'}, function(err, response){
                    if (err) {
                        console.log(err);
                        res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }else{
                        res.jsonp(response);
                    }
                });

            }
        });
};

exports.workLoggedDay = function(req, res){
    //console.log(req.params);
    //console.log(Date(req.params.date));
    console.log(req.params.projectId);
    console.log(req.params.date);
    Budget
        .aggregate()
        .match({projectid: new mongoose.Types.ObjectId(req.params.projectId)})
        .match({created: {$gte: new Date(moment().subtract(1, 'days').toDate()), $lt: new Date(req.params.date)}})
        .group({_id: '$user', hours:{$sum: '$hours'}})
        .project({user: '$_id', hours: '$hours'})
        .sort('_id')
        .exec(function(err, logs) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                //res.jsonp(logs);
                Budget.populate(logs, {path: 'user'}, function(err, response){
                    if (err) {
                        console.log(err);
                        res.status(400).send({
                            message: errorHandler.getErrorMessage(err)
                        });
                    }else{
                        res.jsonp(response);
                    }
                });

            }
        });
};
