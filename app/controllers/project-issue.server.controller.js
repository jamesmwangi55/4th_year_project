'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    ProjectIssue = mongoose.model('ProjectIssue'),
    Log = mongoose.model('Log'),
    _ = require('lodash');

/**
 * Create a Project issue
 */
exports.create = function(req, res) {
    var projectIssue = new ProjectIssue(req.body);
    projectIssue.user = req.user;

    projectIssue.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(projectIssue);
        }
    });
};

/**
 * Show the current Project issue
 */
exports.read = function(req, res) {
    res.jsonp(req.projectIssue);
};

/**
 * Update a Project issue
 */
exports.update = function(req, res) {
    var projectIssue = req.projectIssue ;
    console.log(projectIssue);

    projectIssue = _.extend(projectIssue , req.body);

    projectIssue.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(projectIssue);
        }
    });
};

/**
 * Delete an Project issue
 */
exports.delete = function(req, res) {

};

/**
 * List of Project issues
 */
exports.list = function(req, res) {
    ProjectIssue.find().populate('project').populate('issue').exec(function(err, projectIssues) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(projectIssues);
        }
    });
};


/**
 * ProjectIssue middleware
 */
exports.projectIssueByID = function(req, res, next, id) {
    ProjectIssue.findById(id).populate('project').populate('issue').exec(function(err, projectIssue) {
        if (err) return next(err);
        if (! projectIssue) return next(new Error('Failed to load ProjectIssue ' + id));
        req.projectIssue = projectIssue ;
        next();
    });
};

exports.calculateBudget = function(){
    console.log("this is a test");
};

// aggregate the results
exports.projectIssueAggregate = function(req, res){
    console.log(req.params);
    ProjectIssue.aggregate()
        .group({_id: '$issue.status'})
        .sort('_id')
        .exec(function(err, projectIssues) {
        if (err) {
            console.log(err);
            res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(projectIssues);
        }
    });
};
