'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Log = mongoose.model('Log'),
    _ = require('lodash');

/**
 * Create a Log
 */
exports.create = function(req, res) {
    var log = new Log(req.body);
    log.user = req.user;

    log.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(log);
        }
    });
};

/**
 * Show the current Log
 */
exports.read = function(req, res) {
    res.jsonp(req.log);
};

/**
 * Update a Log
 */
exports.update = function(req, res) {
    var log = req.log ;

    log = _.extend(log , req.body);

    log.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(log);
        }
    });
};

/**
 * Delete an Log
 */
exports.delete = function(req, res) {

};

/**
 * List of Logs
 */
exports.list = function(req, res) {
    Log.find().sort('-logged').populate('user', 'displayName').populate('issue').exec(function(err, logs) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(logs);
        }
    });
};

/**
 * Log middleware
 */
exports.logByID = function(req, res, next, id) {
    Log.findById(id).populate('user', 'displayName').populate('issue').populate('column.issues').exec(function(err, log) {
        if (err) return next(err);
        if (! log) return next(new Error('Failed to load Log ' + id));
        req.log = log ;
        next();
    });
};

//log aggregate
//get hours logged for project
exports.workLogged = function(req, res){
    //console.log(req.params);
    //console.log(Date(req.params.date));
    console.log(req.params.projectId);
    Log
        .aggregate()
        .match({project: new mongoose.Types.ObjectId(req.params.projectId)})
        .group({_id: '$user', hours:{$sum: '$time'}})
        .sort('_id')
        .exec(function(err, logs) {
            if (err) {
                console.log(err);
                res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.jsonp(logs);
            }
        });
};
